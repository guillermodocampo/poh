package poh.com.poh.signup;

import javax.inject.Inject;

import poh.com.poh.base.BaseView;
import poh.com.poh.base.Presenter;
import poh.com.poh.utils.Validations;

public class SignUpPresenter<T extends BaseView> extends Presenter<T> implements SignUpContract.Presenter<T> {


    SignUpContract.View view;

    @Inject
    public SignUpPresenter(){ super();}

    @Override
    public void start(BaseView view) {
        this.view = (SignUpContract.View) view;
    }

    @Override
    public void signUp(String nickname, String name, String lastName, String email, String pass) {
        if (!Validations.validateEmail(email)) {
            view.showEmailError();
        } else{
            view.removeEmailError();
            if (!Validations.validatePassword(pass)) {
                view.showPassError();
            } else {
                view.removePassError();
                //TODO
                /* CALL Service SignUp */
                // Si se registró, agrego al sharedPref y voy al main.
                view.goToMain();
            }
        }
    }

}
