package poh.com.poh.porjectdetails;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.common.base.Strings;
import com.poh.thinkup.thinkuppohmodel.FullProject;
import com.poh.thinkup.thinkuppohmodel.Project;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPFragment;
import poh.com.poh.utils.ExtraKeys;
import poh.com.poh.utils.ImageLoader;
import poh.com.poh.utils.ShareManager;
import poh.com.poh.utils.SpannableBuilder;
import poh.com.poh.videoplayer.VideoPlayActivity;

public class ProjectDetailFragment extends BaseMVPFragment implements ProjectDetailContract.View {

    @BindView(R.id.nested_container)
    NestedScrollView nestedScrollView;
    @BindView(R.id.about_container)
    LinearLayout aboutLinearLayout;
    @BindView(R.id.animation_view)
    protected LottieAnimationView loaderAnimationView;
    @BindView(R.id.profile_header_image)
    ImageView headerImageView;
    @BindView(R.id.profile_media)
    ImageView mediaImageView;
    @BindView(R.id.profile_collaborators_text_view)
    TextView collaboratorsTextView;
    @BindView(R.id.profile_about_text_view)
    TextView aboutTextView;
    @BindView(R.id.project_detail_name)
    TextView nameTextView;
    @BindView(R.id.category_name_project)
    TextView categoryTextView;
    @BindView(R.id.profile_favorite)
    ImageView favoriteImageView;
    @BindView(R.id.profile_category)
    ImageView categoryImageView;

    @Inject
    ProjectDetailPresenter presenter;
    @Inject
    ImageLoader imageLoader;
    @Inject
    SpannableBuilder spannableBuilder;
    @Inject
    ShareManager shareManager;

    private String url;

    public static ProjectDetailFragment newInstance(Project project) {
        ProjectDetailFragment fragment = new ProjectDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ExtraKeys.PROJECT_KEY, project);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.project_detail_fragment, container, false);
        ButterKnife.bind(this, root);
        getPresenter().start(this);
        getPresenter().init((Project) getArguments().getSerializable(ExtraKeys.PROJECT_KEY));
        return root;
    }

    @Override
    protected ProjectDetailContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @OnClick(R.id.profile_media)
    public void onMediaClick() {
        Intent intent = VideoPlayActivity.getIntent(getActivity(), url, true);
        startActivity(intent);
    }

    @OnClick(R.id.profile_about_text_view)
    public void onDescriptionClick() {
        getPresenter().readMoreClicked();
    }

    @OnClick(R.id.fb_item)
    public void onFacebookClick() {
        getPresenter().share(ShareManager.ShareTarget.FACEBOOK);
    }

    @OnClick(R.id.tw_item)
    public void onTwitterClick() {
        getPresenter().share(ShareManager.ShareTarget.TWITTER);
    }

    @OnClick(R.id.ig_item)
    public void onInstagramClick() {
        getPresenter().share(ShareManager.ShareTarget.INSTAGRAM);
    }

    @OnClick(R.id.mail_item)
    public void onMailClick() {
        getPresenter().share(ShareManager.ShareTarget.MAIL);
    }

    @Override
    public void hideLoader() {
        loaderAnimationView.pauseAnimation();
        loaderAnimationView.setVisibility(View.GONE);
    }

    @Override
    public void loadInfo(String name, String category) {
        nameTextView.setText(name);
        categoryTextView.setText(category);
    }

    @Override
    public void loadHeader(String url) {
        imageLoader
                .load(url)
                .into(headerImageView);
    }

    @Override
    public void loadMedia(String url) {
        mediaImageView.setVisibility(!Strings.isNullOrEmpty(url) ? View.VISIBLE : View.GONE);
        this.url = url;
    }

    @Override
    public void loadCollaborators(double totalCollaboration, int humans, int sponsors) {
        collaboratorsTextView.setText(spannableBuilder.createSpannableCollaboration(totalCollaboration, humans, sponsors));
    }

    @Override
    public void loadDescription(String description) {
        aboutTextView.setText(spannableBuilder.createSpannableDescription(description));
    }

    @Override
    public void loadFullDescription(String description) {
        aboutTextView.setText(spannableBuilder.createSpannableFullDescription(description));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nestedScrollView.smoothScrollTo((int) aboutLinearLayout.getX(), (int) aboutLinearLayout.getY());
            }
        }, 200);

    }

    @Override
    public void share(FullProject project, ShareManager.ShareTarget target) {
        shareManager.shareProject(project, target);
    }
}
