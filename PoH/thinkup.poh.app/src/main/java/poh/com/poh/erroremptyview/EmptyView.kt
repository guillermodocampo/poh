package poh.com.poh.erroremptyview

import android.content.Context
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import poh.com.poh.extensions.empty
import poh.com.poh.extensions.notNull

class EmptyView(val context: Context, val root: ViewGroup) {
    @DrawableRes
    private var icon: Int? = null
    @StringRes
    private var title: Int? = null
    @StringRes
    private var description: Int? = null
    @StringRes
    private var action: Int? = null
    private lateinit var listener: () -> Unit

    fun icon(@DrawableRes icon: Int): EmptyView {
        this.icon = icon
        return this
    }

    fun title(@StringRes title: Int): EmptyView {
        this.title = title
        return this
    }

    fun description(@StringRes description: Int): EmptyView {
        this.description = description
        return this
    }

    fun action(@StringRes action: Int): EmptyView {
        this.action = action
        return this
    }

    fun listener(listener: () -> Unit): EmptyView {
        this.listener = listener
        return this
    }

    fun show(): CustomEmptyView {
        val customEmptyView = CustomEmptyView(this, createModel()) { listener.invoke() }
        root.addView(customEmptyView, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        return customEmptyView
    }

    private fun createModel(): EmptyViewModel {
        val iconDrawable = if (icon.notNull()) ContextCompat.getDrawable(context, icon!!) else null
        val titleString = if (title.notNull()) context.getString(title!!) else String.empty()
        val descriptionString = if (description.notNull()) context.getString(description!!) else String.empty()
        val actionString = if (action.notNull()) context.getString(action!!) else String.empty()
        return EmptyViewModel(iconDrawable, titleString, descriptionString, actionString)
    }
}