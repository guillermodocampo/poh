package poh.com.poh.base;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import javax.inject.Inject;

import poh.com.poh.PohApplication;
import poh.com.poh.dagger.components.MainComponent;
import poh.com.poh.utils.ImageLoader;

public abstract class BaseMVPFragment extends Fragment {

    @Inject
    ImageLoader imageLoader;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getPresenter() != null) {
            getPresenter().dropView();
        }
    }

    protected abstract BasePresenter getPresenter();

    public abstract void initializeInjector();

    protected MainComponent getMainComponent() {
        return ((PohApplication) getActivity().getApplication()).getDaggerWrapper().getMainComponent();
    }
}
