package poh.com.poh.splash;

import android.content.SharedPreferences;

import javax.inject.Inject;

import poh.com.poh.base.BaseView;
import poh.com.poh.base.Presenter;

public class SplashPresenter<T extends BaseView> extends Presenter<T> implements SplashContract.Presenter<T> {


    SplashContract.View view;
    SplashContextWrapper scw;

    @Inject
    public SplashPresenter(SplashContextWrapper splashContextWrapper) {
        super();
        this.scw = splashContextWrapper;
    }

    @Override
    public void start(BaseView view) {
        this.view = (SplashContract.View) view;
    }

    @Override
    public void dropView() {

    }

    @Override
    public void checkUserLogged() {
        SharedPreferences pref = scw.getPreferencesUser();
        if(!pref.getBoolean("isLogged", false)){
            view.goToLogin();
        }
        else{
            view.goToMain();
        }
    }
}
