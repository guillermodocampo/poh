package poh.com.poh.signup;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;

public interface SignUpContract {

    interface Presenter<T extends BaseView> extends BasePresenter<T> {
        void signUp(String nickname, String name, String lastName, String email, String pass);
    }

    interface View extends BaseView {
        void showEmailError();
        void removeEmailError();
        void showPassError();
        void removePassError();

        void goToMain();
    }
}
