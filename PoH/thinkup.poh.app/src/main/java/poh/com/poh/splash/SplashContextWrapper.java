package poh.com.poh.splash;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class SplashContextWrapper {

    Context context;

    @Inject
    public SplashContextWrapper(Context context){
        this.context = context;
    }

    public SharedPreferences getPreferencesUser(){
        return context.getSharedPreferences("userPref", context.MODE_PRIVATE);
    }
}
