package poh.com.poh;

import android.app.Application;

import com.poh.thinkup.thinkuppohserivces.lib.Configuration;
import com.poh.thinkup.thinkuppohserivces.lib.PoHServices;

import poh.com.poh.dagger.DaggerWrapper;

public class PohApplication extends Application {

    private DaggerWrapper daggerWrapper;

    @Override
    public void onCreate() {
        super.onCreate();

        PoHServices.init(new Configuration(Configuration.Environment.DEVELOPMENT), getApplicationContext());
        daggerWrapper = new DaggerWrapper();
        daggerWrapper.init(this);
    }

    public DaggerWrapper getDaggerWrapper() {
        return daggerWrapper;
    }
}
