package poh.com.poh.login;

import com.poh.thinkup.thinkuppohserivces.lib.PoHServices;

import javax.inject.Inject;
import poh.com.poh.base.BaseView;
import poh.com.poh.base.Presenter;
import poh.com.poh.utils.Validations;

public class LoginPresenter<T extends BaseView> extends Presenter<T> implements LoginContract.Presenter<T> {


    LoginContract.View view;

    @Inject
    public LoginPresenter() {
        super();
    }


    @Override
    public void start(BaseView view) {
        this.view = (LoginContract.View) view;
    }

    @Override
    public void dropView() {

    }

    @Override
    public void logIn(String email, String pass) {

        boolean coso = Validations.validateEmail(email);
        if (!Validations.validateEmail(email)) {
            view.showEmailError();
        } else{
            view.removeEmailError();
            if (!Validations.validatePassword(pass)) {
                view.showPassError();
            } else {
                view.removePassError();
                //TODO
                PoHServices.updateToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvcG9oLnV5XC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU0NzY4MDk1NiwiZXhwIjoxNTQ3Njg0NTU2LCJuYmYiOjE1NDc2ODA5NTYsImp0aSI6ImxLR0RGZmhFbUc2dTJKQnEiLCJzdWIiOjIsInBydiI6ImYwYjM5OTBlZGUzYWQxOGZiN2Q5ZDRhOWVkZDMyNDFhMzRiMjc0ZTEifQ.a6ANlid_sBSuBvnJ-YNghsHygej8vqf2fvAEec5Q_sk");
                /* CALL Service login */
                // SI se logueo, agrego al sharedPref y voy al main. Hacer wrapper
                view.goToMain();
            }
        }
    }

    /* Send email to recover pass */
    @Override
    public void sendEmail(String email) {
        if (!Validations.validateEmail(email)) {
            view.showSendEmailError();
        } else{
            view.removeSendEmailError();
                //TODO
                /* CALL Service recover pass */
                /* Enviar al dialog que muestre correcto */
        }
    }

}



