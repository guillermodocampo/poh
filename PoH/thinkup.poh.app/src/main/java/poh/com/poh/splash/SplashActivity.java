package poh.com.poh.splash;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import javax.inject.Inject;

import poh.com.poh.R;
import poh.com.poh.base.BaseMVPActivity;
import poh.com.poh.base.BasePresenter;
import poh.com.poh.login.LoginActivity;
import poh.com.poh.main.MainActivity;

public class SplashActivity extends BaseMVPActivity implements SplashContract.View {

    private static final int SPLASH_TIME = 2000;

    @Inject
    SplashPresenter<SplashContract.View> presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        presenter.start(this);
        checkUserLogged();
    }

    private void checkUserLogged() {

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                presenter.checkUserLogged();
                finish();
            }
        }, SPLASH_TIME);
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
