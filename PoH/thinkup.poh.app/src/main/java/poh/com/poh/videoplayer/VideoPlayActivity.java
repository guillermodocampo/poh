package poh.com.poh.videoplayer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import poh.com.poh.R;
import poh.com.poh.utils.Constants;
import poh.com.poh.utils.ExtraKeys;

public class VideoPlayActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, YouTubePlayer.PlayerStateChangeListener, YouTubePlayer.PlaybackEventListener {

    private static final int RECOVERY_REQUEST = 1;

    @BindView(R.id.video_view)
    protected YouTubePlayerView youTubeView;
    @BindView(R.id.progressBar)
    protected ProgressBar progressBar;
    private YouTubePlayer youTubePlayer;
    private String videoId;
    private int currentMillis = 0;
    CountDownTimer countDownTimer;

    private String url;
    private boolean cancelable;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            url = savedInstanceState.getString(ExtraKeys.VIDEO_URL_KEY);
            cancelable = savedInstanceState.getBoolean(ExtraKeys.VIDEO_CANCELABLE_KEY, false);
            currentMillis = savedInstanceState.getInt(ExtraKeys.VIDEO_CURRENT_TIME, 0);
        } else {
            url = getIntent().getStringExtra(ExtraKeys.VIDEO_URL_KEY);
            cancelable = getIntent().getBooleanExtra(ExtraKeys.VIDEO_CANCELABLE_KEY, false);
            currentMillis = getIntent().getIntExtra(ExtraKeys.VIDEO_CURRENT_TIME, 0);
        }

        videoId = extractYoutubeId(url);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (youTubePlayer == null) {
            youTubeView.initialize(Constants.YOUTUBE_API_KEY, this);
        } else {
            youTubePlayer.loadVideo(videoId, currentMillis);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        bundle.putString(ExtraKeys.VIDEO_URL_KEY, url);
        bundle.putInt(ExtraKeys.VIDEO_CURRENT_TIME, currentMillis);
        bundle.putBoolean(ExtraKeys.VIDEO_CANCELABLE_KEY, cancelable);
        super.onSaveInstanceState(bundle);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(Constants.YOUTUBE_API_KEY, this);
        }
    }

    @Override
    public void onBackPressed() {
        if (cancelable) {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    private String extractYoutubeId(String url) {
        String[] parts = url.split("/");

        return parts[parts.length - 1];
    }

    private void showError(String message) {
        String error = String.format("Error: %s", message);
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer ytPlayer, boolean wasRestored) {
        if (!wasRestored) {
            youTubePlayer = ytPlayer;
            youTubePlayer.setPlayerStyle(cancelable ? YouTubePlayer.PlayerStyle.DEFAULT : YouTubePlayer.PlayerStyle.CHROMELESS);
            youTubePlayer.setShowFullscreenButton(false);
            youTubePlayer.setPlayerStateChangeListener(this);
            youTubePlayer.setPlaybackEventListener(this);
            youTubePlayer.loadVideo(videoId);
            youTubePlayer.seekToMillis(currentMillis);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            showError(errorReason.toString());
        }
    }

    @Override
    public void onLoading() {
    }

    @Override
    public void onLoaded(String s) {
        if (!cancelable) {
            progressBar.setVisibility(View.VISIBLE);
            int max = youTubePlayer.getDurationMillis();
            progressBar.setMax(max);
            progressBar.setProgress(currentMillis);
            countDownTimer = new CountDownTimer(max - currentMillis, 50) {
                @Override
                public void onTick(long millisUntilFinished) {
                    progressBar.setProgress((int) millisUntilFinished);
                }

                @Override
                public void onFinish() {

                }
            };
        }
    }

    @Override
    public void onAdStarted() {

    }

    @Override
    public void onVideoStarted() {
        if (!cancelable)
            countDownTimer.start();
    }

    @Override
    public void onVideoEnded() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {
        showError(errorReason.toString());
    }

    @Override
    public void onPlaying() {
    }

    @Override
    public void onPaused() {
        if (!cancelable) {
            countDownTimer.cancel();
        }
        currentMillis = youTubePlayer.getCurrentTimeMillis();
    }

    @Override
    public void onStopped() {
    }

    @Override
    public void onBuffering(boolean b) {

    }

    @Override
    public void onSeekTo(int current) {

    }

    public static Intent getIntent(Activity source, String url, boolean cancelable) {
        Intent intent = new Intent(source, VideoPlayActivity.class);
        intent.putExtra(ExtraKeys.VIDEO_URL_KEY, url);
        intent.putExtra(ExtraKeys.VIDEO_CANCELABLE_KEY, cancelable);

        return intent;
    }
}
