package poh.com.poh.signup;

import android.app.Activity;
import android.content.Intent;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.content.res.ResourcesCompat;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPActivity;
import poh.com.poh.login.LoginActivity;
import poh.com.poh.main.MainActivity;

public class SignUpActivity extends BaseMVPActivity implements SignUpContract.View {

    @BindView(R.id.nicknameWrapper) protected TextInputLayout nicknameWrapper;
    @BindView(R.id.nameWrapper) protected TextInputLayout nameWrapper;
    @BindView(R.id.lastNameWrapper) protected TextInputLayout lastNameWrapper;
    @BindView(R.id.emailWrapper) protected TextInputLayout emailWrapper;
    @BindView(R.id.passWrapper) protected TextInputLayout passWrapper;

    @BindView(R.id.editTextNickname) protected EditText editTextNickname;
    @BindView(R.id.editTextName) protected EditText editTextName;
    @BindView(R.id.editTextLastName) protected EditText editTextLastName;
    @BindView(R.id.editTextEmail) protected EditText editTextEmail;
    @BindView(R.id.editTextPass) protected EditText editTextPass;

    @BindView(R.id.textViewLogin) protected TextView textViewLogin;
    @BindView(R.id.textViewSignUp) protected TextView textViewEnter;

    @Inject
    SignUpPresenter<SignUpContract.View> presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        setFonts();
        presenter.start(this);
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @Override
    protected SignUpContract.Presenter getPresenter() {
        return presenter;
    }

    /* Set TextInputLayout's fonts.
     * Have to do it by code becase styles not support two different fonts, one for the editTExt
     * and one for the hint.*/
    private void setFonts() {
        nicknameWrapper.setTypeface(ResourcesCompat.getFont(this, R.font.futura_bold));
        nameWrapper.setTypeface(ResourcesCompat.getFont(this, R.font.futura_bold));
        lastNameWrapper.setTypeface(ResourcesCompat.getFont(this, R.font.futura_bold));
        emailWrapper.setTypeface(ResourcesCompat.getFont(this, R.font.futura_bold));
        passWrapper.setTypeface(ResourcesCompat.getFont(this, R.font.futura_bold));
    }

    @OnClick({R.id.textViewLogin})
    public void goToLogin(View view){
        Intent intent = new Intent(view.getContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.textViewSignUp)
    public void signUp(View view){
        String nickname = "", name = "",last_name = "", email = "", password = "";
        if(areNotNull()){
            nickname = nicknameWrapper.getEditText().getText().toString();
            name = nameWrapper.getEditText().getText().toString();
            last_name = lastNameWrapper.getEditText().getText().toString();
            email = emailWrapper.getEditText().getText().toString();
            password = passWrapper.getEditText().getText().toString();
        }
        presenter.signUp(nickname, name, last_name, email, password);
    }

    /* All wrappers are not null */
    private boolean areNotNull(){
        return nicknameWrapper.getEditText() != null && nameWrapper.getEditText() != null
                && lastNameWrapper.getEditText() != null && emailWrapper.getEditText() != null
                && passWrapper.getEditText() != null;
    }

    /* Change color and enables bottom button. */
    @OnTextChanged({R.id.editTextNickname, R.id.editTextName, R.id.editTextLastName,
            R.id.editTextEmail, R.id.editTextPass})
    public void changeTextNickname(){
        if (areNotEmpty()){
            textViewEnter.setEnabled(true);
            textViewEnter.setTextColor(getResources().getColor(R.color.main_projects));
        }
        else{
            textViewEnter.setEnabled(false);
            textViewEnter.setTextColor(getResources().getColor(R.color.main_projects_30));
        }
    }

    /* All editText are not empty */
    private boolean areNotEmpty(){
        return !editTextNickname.getText().toString().equals("") &&
                !editTextName.getText().toString().equals("") &&
                !editTextLastName.getText().toString().equals("") &&
                !editTextEmail.getText().toString().equals("") &&
                !editTextPass.getText().toString().equals("");
    }

    @Override
    public void showEmailError() {
        emailWrapper.setError(getString(R.string.error_email));
    }

    @Override
    public void removeEmailError() {
        emailWrapper.setErrorEnabled(false);
    }

    @Override
    public void showPassError() {
        passWrapper.setError(getString(R.string.error_pass));
    }

    @Override
    public void removePassError() {
        passWrapper.setErrorEnabled(false);
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @OnFocusChange({R.id.editTextNickname, R.id.editTextName, R.id.editTextLastName,
            R.id.editTextEmail, R.id.editTextPass})
    public void hideBoard(View view){
        if(!view.hasFocus()){
            hideKeyboard(view);
        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

}
