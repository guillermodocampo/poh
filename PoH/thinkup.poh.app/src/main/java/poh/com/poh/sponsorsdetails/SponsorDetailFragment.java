package poh.com.poh.sponsorsdetails;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.poh.thinkup.thinkuppohmodel.Sponsor;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPFragment;
import poh.com.poh.sponsorselectproject.SponsorSelectProjActivity;
import poh.com.poh.utils.ExtraKeys;
import poh.com.poh.utils.ImageLoader;
import poh.com.poh.utils.SpannableBuilder;

public class SponsorDetailFragment extends BaseMVPFragment implements SponsorDetailContract.View{

    @BindView(R.id.profile_header_image)
    ImageView headerImageView;
    @BindView(R.id.category_name_sponsor)
    TextView categoryTextView;
    @BindView(R.id.sponsor_name)
    TextView nameTextView;
    @BindView(R.id.profile_collaborators_text_view)
    TextView collaboratorsTextView;
    @BindView(R.id.animation_view_image)
    protected LottieAnimationView animationViewImage;
    @BindView(R.id.animation_view_profile)
    protected LottieAnimationView animationViewProfile;
    @BindView(R.id.animation_view_about)
    protected LottieAnimationView animationViewAbout;
    @BindView(R.id.profile_about_text_view)
    TextView aboutTextView;
    @BindView(R.id.nested_container)
    NestedScrollView nestedScrollView;
    @BindView(R.id.about_container)
    LinearLayout aboutLinearLayout;
    @BindView(R.id.view_gradient)
    View viewGradient;
    @BindView(R.id.profile_favorite)
    ImageView profileFavorite;



    @Inject
    SponsorDetailPresenter presenter;
    @Inject
    ImageLoader imageLoader;
    @Inject
    SpannableBuilder spannableBuilder;

    public static SponsorDetailFragment newInstance(Sponsor sponsor) {
        SponsorDetailFragment fragment = new SponsorDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ExtraKeys.SPONSOR_KEY, sponsor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.sponsor_details_fragment, container, false);
        ButterKnife.bind(this, root);
        getPresenter().start(this);
        getPresenter().init((Sponsor) getArguments().getSerializable(ExtraKeys.SPONSOR_KEY));
        return root;
    }

    @Override
    protected SponsorDetailContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @OnClick(R.id.profile_about_text_view)
    public void onDescriptionClick() {
        getPresenter().readMoreClicked();
    }


    @Override
    public void hideLoader() {
        animationViewImage.pauseAnimation();
        animationViewImage.setVisibility(View.GONE);
        animationViewProfile.pauseAnimation();
        animationViewProfile.setVisibility(View.GONE);
        animationViewAbout.pauseAnimation();
        animationViewAbout.setVisibility(View.GONE);
    }

    @Override
    public void loadInfo(String sponsorName, String category) {
        categoryTextView.setText(category);
        nameTextView.setText(sponsorName);
    }

    @Override
    public void loadHeader(String url) {
        imageLoader.load(url).into(headerImageView);
    }

    @Override
    public void loadCollaborators(double totalDonated, int projects, int humans) {
        collaboratorsTextView.setText(spannableBuilder.createSpannableCollaborationSponsor(totalDonated, projects, humans));
    }

    @Override
    public void loadDescription(String description) {
        aboutTextView.setText(spannableBuilder.createSpannableDescription(description));
    }

    @Override
    public void loadFullDescription(String description) {
        aboutTextView.setText(spannableBuilder.createSpannableFullDescription(description));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nestedScrollView.smoothScrollTo((int) aboutLinearLayout.getX(), (int) aboutLinearLayout.getY());
            }
        }, 200);
    }

    @Override
    public void showHeader() {
        viewGradient.setVisibility(View.VISIBLE);
        profileFavorite.setVisibility(View.VISIBLE);
        categoryTextView.setVisibility(View.VISIBLE);
        nameTextView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.click_select_project)
    public void goToSelectProject() {
        Intent intent = SponsorSelectProjActivity.getIntent(getActivity(), presenter.getSponsor(), presenter.getFullSponsor());
        startActivity(intent);
    }
}
