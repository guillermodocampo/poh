package poh.com.poh.main;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import poh.com.poh.PohApplication;
import poh.com.poh.R;
import poh.com.poh.dagger.components.MainComponent;
import poh.com.poh.utils.UIUtils;

public class CustomMenu extends ConstraintLayout {

    @BindView(R.id.context_menu_layout)
    ConstraintLayout mainConstraintLayout;
    @BindView(R.id.menu_projects)
    TextView menuProjectsTextView;
    @BindView(R.id.menu_me)
    TextView menuMeTextView;
    @BindView(R.id.menu_sponsors)
    TextView menuSponsorTextView;
    @BindView(R.id.menu_be_sponsors)
    TextView menuBeSponsorTextView;
    @BindView(R.id.menu_call_sponsors)
    TextView menuCallSponsorTextView;
    @BindView(R.id.menu_logout)
    TextView menuLogoutTextView;

    @Inject
    UIUtils uiUtils;

    private MenuListener listener;

    public CustomMenu(Context context) {
        super(context);
        init();
    }

    public CustomMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        getDaggerComponent().inject(this);
        View view = inflate(getContext(), R.layout.custom_menu, this);

        ButterKnife.bind(this, view);
    }

    public void setListener(MenuListener listener) {
        this.listener = listener;
    }

    public void setVisible(int position, boolean visibility) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_logout);
        drawable.setColorFilter(uiUtils.getLogoutColor(position), PorterDuff.Mode.SRC_IN);

        menuLogoutTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null, null, null);
        uiUtils.animateBackground(mainConstraintLayout, position, visibility);
        menuLogoutTextView.setTextColor(uiUtils.getLogoutColor(position));
        setVisible(visibility);
    }

    public void changeItemsColor(int position) {
        uiUtils.animateTextViews(position, menuProjectsTextView, menuSponsorTextView, menuMeTextView, menuBeSponsorTextView, menuCallSponsorTextView);
    }

    public void setVisible(boolean visibility) {
        mainConstraintLayout.setVisibility(visibility ? GONE : VISIBLE);
    }

    @OnClick({R.id.menu_projects, R.id.menu_me, R.id.menu_sponsors})
    public void onItemClick(View view) {
        if (listener != null) {
            listener.onMenuItemClick(view.getId());
        }
    }

    private MainComponent getDaggerComponent() {
        return ((PohApplication) getContext().getApplicationContext()).getDaggerWrapper().getMainComponent();
    }

    public interface MenuListener {
        void onMenuItemClick(int id);
    }
}
