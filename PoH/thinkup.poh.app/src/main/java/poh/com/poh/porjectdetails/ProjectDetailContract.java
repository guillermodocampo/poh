package poh.com.poh.porjectdetails;

import com.poh.thinkup.thinkuppohmodel.FullProject;
import com.poh.thinkup.thinkuppohmodel.Project;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;
import poh.com.poh.utils.ShareManager;

public interface ProjectDetailContract {
    interface Presenter<T extends BaseView> extends BasePresenter<T> {
        void init(Project project);
        void readMoreClicked();
        void share(ShareManager.ShareTarget target);
    }

    interface View extends BaseView {
        void hideLoader();
        void loadInfo(String name, String category);
        void loadHeader(String url);
        void loadMedia(String url);
        void loadCollaborators(double totalCollaboration, int humans, int sponsors);
        void loadDescription(String description);
        void loadFullDescription(String description);
        void share(FullProject project, ShareManager.ShareTarget target);
    }
}
