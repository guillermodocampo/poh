package poh.com.poh.sponsors.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.poh.thinkup.thinkuppohmodel.Sponsor;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import poh.com.poh.R;

public class SponsorsAdapter extends RecyclerView.Adapter<SponsorsAdapter.SponsorsViewHolder> {

    private List<Sponsor> sponsors;
    private SponsorClick sponsorClick;

    @Inject
    public SponsorsAdapter(){}

    public void init(List<Sponsor> sponsorList, SponsorClick sponsorClick){
        this.sponsors = sponsorList;
        this.sponsorClick = sponsorClick;
    }

    @NonNull
    @Override
    public SponsorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sponsor_all, parent, false);
        return new SponsorsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SponsorsViewHolder holder, int position) {
        final Sponsor sponsor = sponsors.get(position);
        holder.bind(sponsors.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sponsorClick.onSponsorClick(sponsor);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sponsors != null ? sponsors.size() : 0;
    }

    public class SponsorsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_sponsor_name)
        TextView sponsorName;

        public SponsorsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Sponsor sponsor){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sponsorClick.onSponsorClick(sponsor);
                }
            });
            sponsorName.setText(sponsor.getSponsorName());
        }
    }
}
