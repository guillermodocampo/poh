package poh.com.poh.utils;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.annotation.DrawableRes;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import java.io.File;

import javax.inject.Inject;

public class ImageLoader {

    private RequestCreator request;
    private com.squareup.picasso.Callback callback;

    @Inject
    public ImageLoader() {
    }

    public ImageLoader load(String string) {
        request = Picasso.get().load(string);
        return this;
    }

    public ImageLoader load(Uri uri) {
        request = Picasso.get().load(uri);
        return this;
    }

    public ImageLoader load(int resourceId) {
        request = Picasso.get().load(resourceId);
        return this;
    }

    public ImageLoader load(File file) {
        request = Picasso.get().load(file);
        return this;
    }

    public ImageLoader placeholder(@DrawableRes int resourceId) {
        request = request.placeholder(resourceId);
        return this;
    }

    public ImageLoader placeholder(Drawable drawable) {
        request = request.placeholder(drawable);
        return this;
    }

    public ImageLoader error(@DrawableRes int resourceId) {
        request = request.error(resourceId);
        return this;
    }

    public ImageLoader callback(final Callback callback) {
        this.callback = new Callback() {
            public void onSuccess() {
                callback.onSuccess();
            }

            public void onError(Exception e) {
                callback.onError(e);
            }
        };
        return this;
    }

    public ImageLoader transform(final Transformation transformation) {
        request = request.transform(new Transformation() {
            public Bitmap transform(Bitmap source) {
                return transformation.transform(source);
            }

            public String key() {
                return transformation.key();
            }
        });
        return this;
    }

    public void cancelLoad(ImageView imageView) {
        Picasso.get().cancelRequest(imageView);
    }

    public void into(ImageView imageView) {
        if (callback != null) {
            request.into(imageView, callback);
        } else {
            request.into(imageView);
        }
    }

    public void into(final Target target) {
        request.into(new Target() {
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                target.onBitmapLoaded(bitmap, from);
            }

            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                target.onBitmapFailed(e, errorDrawable);
            }

            public void onPrepareLoad(Drawable placeHolderDrawable) {
                target.onPrepareLoad(placeHolderDrawable);
            }
        });
    }
}
