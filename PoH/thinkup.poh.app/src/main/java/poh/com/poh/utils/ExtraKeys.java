package poh.com.poh.utils;

public class ExtraKeys {
    public static final String PROJECT_KEY = "project_key";
    public static final String VIDEO_URL_KEY = "video_url_key";
    public static final String VIDEO_CANCELABLE_KEY = "video_cancelable_key";
    public static final String VIDEO_CURRENT_TIME = "video_current_time";

    public static final String SPONSOR_KEY = "sponsor_key";
    public static final String FULL_SPONSOR_KEY = "full_sponsor_key";
}
