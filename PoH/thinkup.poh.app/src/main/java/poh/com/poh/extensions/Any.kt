package poh.com.poh.extensions

fun Any?.notNull(): Boolean {
    return this != null
}

fun Any?.isNull(): Boolean {
    return this == null
}