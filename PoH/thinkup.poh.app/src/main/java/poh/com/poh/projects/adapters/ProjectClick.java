package poh.com.poh.projects.adapters;

import com.poh.thinkup.thinkuppohmodel.Project;

public interface ProjectClick {
    void onProjectClick(Project project);
}
