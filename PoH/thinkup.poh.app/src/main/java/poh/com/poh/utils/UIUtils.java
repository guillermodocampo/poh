package poh.com.poh.utils;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import poh.com.poh.R;

public class UIUtils {

    private static final int DURATION = 500;

    private Context context;

    @Inject
    public UIUtils(Context context) {
        this.context = context;
    }

    public void animateBackgroundPosition(View view, int fromPosition, int toPosition) {
        animateColorBackground(view, getColor(fromPosition), getColor(toPosition));
    }

    public void animateBackground(View view, int position, boolean menuStatus) {
        @ColorInt int from = menuStatus ? getMenuColor(position) : getColor(position);
        @ColorInt int to = menuStatus ? getColor(position) : getMenuColor(position);
        animateBackground(view, from, to);
    }

    public void animateTextViews(int position, @NonNull TextView... views) {
        int finalColor = position == 0 ? Color.WHITE : Color.BLACK;
        int previousColor = position != 0 ? Color.WHITE : Color.BLACK;
        for (TextView textView : views) {
            animateTextColor(textView, previousColor, finalColor);
        }
    }

    private void animateTextColor(TextView view, @ColorInt int from, @ColorInt int to) {
        ObjectAnimator.ofObject(
                view, // Object to animating
                "textColor", // Property to animate
                new ArgbEvaluator(), // Interpolation function
                from, // Start color
                to// End color
        )
                .setDuration(DURATION) // Duration in milliseconds
                .start();
    }

    private void animateBackground(View view, @ColorInt int from, @ColorInt int to) {
        ValueAnimator colorAnim = ObjectAnimator.ofInt(view, "backgroundColor", from, to);
        colorAnim.setDuration(DURATION);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.start();
    }

    private void animateColorBackground(View view, @ColorInt int fromColor, @ColorInt int toColor) {
        ColorDrawable fromDrawableColor = new ColorDrawable(fromColor);
        ColorDrawable toDrawableColor = new ColorDrawable(toColor);

        ColorDrawable[] color = {fromDrawableColor, toDrawableColor};
        TransitionDrawable trans = new TransitionDrawable(color);
        view.setBackground(trans);
        trans.startTransition(DURATION);
    }

    @ColorInt
    public int getLogoutColor(int position) {
        switch (position) {
            case Constants.HUMANS_INDEX:
                return context.getResources().getColor(R.color.unselected_human_tabs);
            case Constants.SPONSORS_INDEX:
                return context.getResources().getColor(R.color.unselected_sponsor_tabs);
            case Constants.PROJECTS_INDEX:
            default:
                return context.getResources().getColor(R.color.unselected_project_tabs);
        }
    }

    @ColorInt
    private int getColor(int position) {
        switch (position) {
            case Constants.HUMANS_INDEX:
                return context.getResources().getColor(R.color.main_humans);
            case Constants.SPONSORS_INDEX:
                return context.getResources().getColor(R.color.main_sponsors);
            case Constants.PROJECTS_INDEX:
            default:
                return context.getResources().getColor(R.color.main_projects);
        }
    }

    @ColorInt
    private int getMenuColor(int position) {
        switch (position) {
            case Constants.HUMANS_INDEX:
                return context.getResources().getColor(R.color.menu_humans);
            case Constants.SPONSORS_INDEX:
                return context.getResources().getColor(R.color.menu_sponsors);
            case Constants.PROJECTS_INDEX:
            default:
                return context.getResources().getColor(R.color.menu_projects);
        }
    }
}
