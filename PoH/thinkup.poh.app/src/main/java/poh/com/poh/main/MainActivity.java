package poh.com.poh.main;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import com.jaeger.library.StatusBarUtil;
import com.poh.thinkup.thinkuppohmodel.Category;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPActivity;
import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.RxBus;
import poh.com.poh.human.HumanFragment;
import poh.com.poh.projects.ProjectsFragment;
import poh.com.poh.projects.bycategory.CategoryProjectsFragment;
import poh.com.poh.sponsors.SponsorsFragment;
import poh.com.poh.utils.UIUtils;

public class MainActivity extends BaseMVPActivity implements CustomBottomBar.TabSelectedChange, CustomToolbar.ToolbarListener {

    @BindView(R.id.bottom_bar_menu)
    CustomBottomBar customBottomBar;
    @BindView(R.id.custom_toolbar_control)
    CustomToolbar customToolbar;

    @Inject
    UIUtils uiUtils;

    private Disposable rxBusDisposable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        StatusBarUtil.setTransparent(this);

        customBottomBar.setClickListener(this);
        customToolbar.setClickListener(this);
        customToolbar.setUtils(getSupportFragmentManager());

        inflateProjects();
        subscribeEventBus();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        rxBusDisposable.dispose();
    }

    private void inflateProjects() {
        inflateFragment(ProjectsFragment.newInstance());
    }

    private void inflateHuman() {
        inflateFragment(HumanFragment.newInstance());
    }

    private void inflateSponsors() {
        inflateFragment(SponsorsFragment.newInstance());
    }

    private void inflateProjects(Category category) {
        customToolbar.replaceFragment(CategoryProjectsFragment.newInstance(category), R.string.categories_back_toolbar);
    }

    private void inflateFragment(Fragment fragment) {
        customToolbar.inflateFragment(fragment);
    }

    private void menuNavigationRestore(int id) {
        customToolbar.restoreMainToolbar();
        menuNavigation(id);
    }

    private void menuNavigation(int id) {
        switch (id) {
            case R.id.menu_me:
            case R.id.tab_me:
                inflateHuman();
                break;
            case R.id.menu_sponsors:
            case R.id.tab_sponsors:
                inflateSponsors();
                break;
            case R.id.menu_projects:
            case R.id.tab_projects:
            default:
                inflateProjects();
                break;
        }
    }

    private void subscribeEventBus() {
        rxBusDisposable = RxBus.getInstance().toObserverable().subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object o) throws Exception {
                if (o instanceof Category) {
                    inflateProjects((Category) o);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (customToolbar.hasReplaceableFragment()) {
            customToolbar.setHasReplaceableFragment(false);
            menuNavigationRestore(R.id.tab_projects);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    public void onSelectTab(int id, int fromPosition, int toPosition) {
        customToolbar.setColor(fromPosition, toPosition);
        customBottomBar.setColor(fromPosition, toPosition);
        menuNavigationRestore(id);
    }

    @Override
    public void onToolbarItemClick(int id) {
        menuNavigationRestore(id);
    }

    @Override
    public void onToolbarItemClick(int id, int from, int to) {
        customToolbar.setColor(from, to);
        customBottomBar.setColor(to);
        menuNavigationRestore(id);
    }
}
