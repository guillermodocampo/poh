package poh.com.poh.projects.bycategory;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.poh.thinkup.thinkuppohmodel.Category;
import com.poh.thinkup.thinkuppohmodel.Project;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPFragment;
import poh.com.poh.porjectdetails.ProjectDetailActivity;
import poh.com.poh.projects.adapters.ProjectClick;
import poh.com.poh.projects.adapters.ProjectsAdapter;

import static androidx.recyclerview.widget.RecyclerView.VERTICAL;

public class CategoryProjectsFragment extends BaseMVPFragment implements CategoryProjectsContract.View, ProjectClick {

    private static final String PARA_CATEGORY = "para_category";

    @BindView(R.id.recycler_view_projects)
    RecyclerView projectsRecyclerView;
    @BindView(R.id.category_title)
    TextView titleTextView;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;

    @Inject
    ProjectsAdapter adapter;
    @Inject
    CategoryProjectsPresenter presenter;

    public static CategoryProjectsFragment newInstance(Category category) {
        CategoryProjectsFragment fragment = new CategoryProjectsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARA_CATEGORY, category);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.category_projects_fragment, container, false);
        ButterKnife.bind(this, root);
        getPresenter().start(this);
        getPresenter().init((Category) getArguments().getParcelable(PARA_CATEGORY));

        initRecyclerView();

        return root;
    }

    private void initRecyclerView() {
        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), VERTICAL);
        decoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.list_divider));
        projectsRecyclerView.addItemDecoration(decoration);
    }

    @Override
    protected CategoryProjectsContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @Override
    public void loadProjects(List<Project> projects) {
        adapter.init(projects, this);
        projectsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        projectsRecyclerView.setAdapter(adapter);
        animationView.setVisibility(View.GONE);
    }

    @Override
    public void loadTitle(String title) {
        titleTextView.setText(title);
    }

    @Override
    public void onProjectClick(Project project) {
        Intent intent = ProjectDetailActivity.getIntent(getActivity(), project);
        startActivity(intent);
    }
}
