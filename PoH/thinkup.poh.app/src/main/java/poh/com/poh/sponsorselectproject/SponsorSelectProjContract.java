package poh.com.poh.sponsorselectproject;

import com.poh.thinkup.thinkuppohmodel.FullSponsor;
import com.poh.thinkup.thinkuppohmodel.Project;

import java.util.List;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;

public interface SponsorSelectProjContract {

    interface Presenter<T extends BaseView> extends BasePresenter<T> {
        void init(FullSponsor fullSponsor);
        void loadSponsor();
    }

    interface View extends BaseView {
        void hideLoader();
        void loadInfo(String sponsorName, String category);
        void loadHeader(String url);
        void loadProjects(List<Project> projects);
        void showHeader();
    }
}
