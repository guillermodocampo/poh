package poh.com.poh.forgot_pass;

public interface ForgotPass {

    void sendEmail(String email);
}
