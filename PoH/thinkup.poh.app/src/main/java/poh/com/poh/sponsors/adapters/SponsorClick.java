package poh.com.poh.sponsors.adapters;

import com.poh.thinkup.thinkuppohmodel.Sponsor;

public interface SponsorClick {
    void onSponsorClick(Sponsor sponsor);
}
