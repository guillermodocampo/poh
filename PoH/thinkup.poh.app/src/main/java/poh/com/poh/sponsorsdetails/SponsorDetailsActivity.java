package poh.com.poh.sponsorsdetails;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.poh.thinkup.thinkuppohmodel.Sponsor;

import butterknife.BindView;
import poh.com.poh.R;
import poh.com.poh.base.BaseMenuActivity;
import poh.com.poh.base.BasePresenter;
import poh.com.poh.main.CustomToolbar;
import poh.com.poh.utils.ExtraKeys;

public class SponsorDetailsActivity extends BaseMenuActivity implements CustomToolbar.ToolbarListener {

    @BindView(R.id.custom_toolbar_control)
    protected CustomToolbar customToolbar;

    private Sponsor sponsor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customToolbar.setTitle(R.string.sponsor_detail_back_toolbar);
        customToolbar.setColor(2,2);

        if (savedInstanceState != null) {
            sponsor = (Sponsor) savedInstanceState.getSerializable(ExtraKeys.SPONSOR_KEY);
        } else {
            sponsor = (Sponsor) getIntent().getSerializableExtra(ExtraKeys.SPONSOR_KEY);
        }

        customToolbar.setNavigationClick(this);
        customToolbar.inflateFragment(SponsorDetailFragment.newInstance(sponsor));
    }

    @Override
    public void initializeInjector() {

    }

    public static Intent getIntent(Activity source, Sponsor sponsor) {
        Intent intent = new Intent(source, SponsorDetailsActivity.class);
        intent.putExtra(ExtraKeys.SPONSOR_KEY, sponsor);
        return intent;
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    public void onToolbarItemClick(int id) {
        super.onBackPressed();
    }

    @Override
    public void onToolbarItemClick(int id, int from, int to) {

    }
}
