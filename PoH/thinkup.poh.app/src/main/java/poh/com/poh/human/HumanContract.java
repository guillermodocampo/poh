package poh.com.poh.human;

import com.poh.thinkup.thinkuppohmodel.Human;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;

public interface HumanContract {
    interface Presenter<T extends BaseView> extends BasePresenter<T> {
        void init();
    }

    interface View extends BaseView {
        void loadHuman(Human human);
    }
}
