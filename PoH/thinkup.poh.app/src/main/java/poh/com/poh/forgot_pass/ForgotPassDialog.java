package poh.com.poh.forgot_pass;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import poh.com.poh.R;

public class ForgotPassDialog extends DialogFragment {

    @BindView(R.id.emailWrapper) protected TextInputLayout emailWrapper;
    @BindView(R.id.editTextEmail) protected EditText editTextEmail;
    @BindView(R.id.buttonSend) protected Button buttonSend;
    @BindView(R.id.buttonCancel) protected Button buttonCancel;

    public ForgotPass forgotPass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_dialog_forgot_pass, container, false);
        ButterKnife.bind(this, view);
        setFonts(view);
        buttonSend.setTextColor(getResources().getColor(R.color.main_projects_30));
        return view;
    }

    private void setFonts(View view) {
        emailWrapper.setTypeface(ResourcesCompat.getFont(view.getContext(), R.font.futura_bold));
    }

    @OnClick(R.id.buttonCancel)
    public void cancel(View view){
        this.dismiss();
    }

    @OnClick(R.id.buttonSend)
    public void sendEmail(View view){
        String email = emailWrapper.getEditText().getText().toString();
        forgotPass.sendEmail(email);
    }

    /* Change color and enables button send. */
    @OnTextChanged({R.id.editTextEmail})
    public void enableButtonSend(){
        if (!editTextEmail.getText().toString().equals("")){
            buttonSend.setEnabled(true);
            buttonSend.setTextColor(getResources().getColor(R.color.main_projects));
        }
        else{
            buttonSend.setEnabled(false);
            buttonSend.setTextColor(getResources().getColor(R.color.main_projects_30));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            forgotPass = (ForgotPass) getActivity();
        } catch (ClassCastException e){
            this.dismiss();
        }
    }

    public void showEmailError(String error){
        emailWrapper.setError(error);
    }

    public void removeEmailError() {
        emailWrapper.setErrorEnabled(false);
    }
}
