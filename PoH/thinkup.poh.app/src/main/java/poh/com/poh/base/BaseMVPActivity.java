package poh.com.poh.base;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import poh.com.poh.PohApplication;
import poh.com.poh.dagger.components.MainComponent;

public abstract class BaseMVPActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (getPresenter() != null) {
            getPresenter().dropView();
        }
    }

    protected MainComponent getMainComponent() {
        return ((PohApplication) getApplication()).getDaggerWrapper().getMainComponent();
    }

    public abstract void initializeInjector();

    protected abstract BasePresenter getPresenter();
}
