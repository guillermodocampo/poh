package poh.com.poh.human;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.poh.thinkup.thinkuppohmodel.Human;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPFragment;
import poh.com.poh.utils.ImageLoader;
import poh.com.poh.utils.SpannableBuilder;

public class HumanFragment extends BaseMVPFragment implements HumanContract.View {

    @BindView(R.id.human_image)
    ImageView profileImageView;
    @BindView(R.id.human_id)
    TextView idTextView;
    @BindView(R.id.human_name)
    TextView nameTextView;
    @BindView(R.id.human_email)
    TextView emailTextView;
    @BindView(R.id.human_achievements_text_view)
    TextView achievementTextView;

    @Inject
    HumanPresenter presenter;
    @Inject
    SpannableBuilder spannableBuilder;
    @Inject
    ImageLoader imageLoader;

    public static HumanFragment newInstance() {
        HumanFragment fragment = new HumanFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.human_fragment, container, false);
        ButterKnife.bind(this, root);
        getPresenter().start(this);

        getPresenter().init();

        return root;
    }

    @OnClick(R.id.human_edit)
    public void onEditClick() {
        Toast.makeText(getContext(), "Not yet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadHuman(Human human) {
        loadInfo();
        loadAchievements();
    }

    private void loadInfo() {
        imageLoader.load("https://media.istockphoto.com/photos/man-looking-away-on-white-background-picture-id184147789?s=2048x2048")
                .into(profileImageView);
        nameTextView.setText("Nicolas Castro");
        idTextView.setText("nicoc_ale");
        emailTextView.setText("nicoc_ale@hotmail.com");
    }

    private void loadAchievements() {
        achievementTextView.setText(spannableBuilder.createSpannableAcievements(1, 10, "2:10", 30.5, 17));
    }

    @Override
    protected HumanContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }
}
