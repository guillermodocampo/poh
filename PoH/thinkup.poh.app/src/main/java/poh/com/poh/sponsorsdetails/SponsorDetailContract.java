package poh.com.poh.sponsorsdetails;

import com.poh.thinkup.thinkuppohmodel.Sponsor;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;

public interface SponsorDetailContract {

    interface Presenter<T extends BaseView> extends BasePresenter<T> {
        void init(Sponsor sponsor);
        void readMoreClicked();
        void loadSponsor();
    }

    interface View extends BaseView {
        void hideLoader();
        void loadInfo(String sponsorName, String category);
        void loadHeader(String url);
        void loadCollaborators(double totalDonated, int projects, int humans);
        void loadDescription(String description);
        void loadFullDescription(String description);
        void showHeader();
    }

}
