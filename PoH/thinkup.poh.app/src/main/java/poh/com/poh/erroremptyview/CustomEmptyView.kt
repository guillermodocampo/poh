package poh.com.poh.erroremptyview

import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import poh.com.poh.R
import poh.com.poh.databinding.CustomEmptyViewBinding

class CustomEmptyView internal constructor(emptyView: EmptyView, model: EmptyViewModel, listener: () -> Unit) : FrameLayout(emptyView.context) {

    var binding: CustomEmptyViewBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_empty_view, this, true)

    init {
        binding.viewModel = model
        binding.emptyViewButton.setOnClickListener {
            listener.invoke()
        }
    }
}