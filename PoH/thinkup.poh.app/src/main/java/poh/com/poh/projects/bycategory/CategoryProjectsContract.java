package poh.com.poh.projects.bycategory;

import com.poh.thinkup.thinkuppohmodel.Category;
import com.poh.thinkup.thinkuppohmodel.Project;

import java.util.List;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;

public interface CategoryProjectsContract {

    interface Presenter<T extends BaseView> extends BasePresenter<T> {
        void init(Category category);
    }

    interface View extends BaseView {
        void loadProjects(List<Project> projects);
        void loadTitle(String title);
    }
}
