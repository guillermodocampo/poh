package poh.com.poh.dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import poh.com.poh.dagger.modules.MainModule;
import poh.com.poh.forgot_pass.ForgotPassDialog;
import poh.com.poh.human.HumanFragment;
import poh.com.poh.main.CustomBottomBar;
import poh.com.poh.main.CustomMenu;
import poh.com.poh.main.CustomToolbar;
import poh.com.poh.main.MainActivity;
import poh.com.poh.porjectdetails.ProjectDetailFragment;
import poh.com.poh.projects.ProjectsFragment;
import poh.com.poh.signup.SignUpActivity;
import poh.com.poh.splash.SplashActivity;
import poh.com.poh.projects.bycategory.CategoryProjectsFragment;
import poh.com.poh.sponsors.SponsorsFragment;
import poh.com.poh.login.LoginActivity;
import poh.com.poh.sponsorsdetails.SponsorDetailFragment;
import poh.com.poh.sponsorselectproject.SponsorSelectProjFragment;

@Singleton
@Component(modules = {MainModule.class})
public interface MainComponent {
    void inject(MainActivity mainActivity);
	void inject(LoginActivity loginActivity);
	void inject(SplashActivity splashActivity);
    void inject(SignUpActivity signUpActivity);
    void inject(ProjectsFragment projectsFragment);
    void inject(HumanFragment humanFragment);
    void inject(SponsorsFragment sponsorsFragment);
    void inject(CategoryProjectsFragment categoryProjectsFragment);
    void inject(SponsorDetailFragment sponsorDetailsFragment);
    void inject(ProjectDetailFragment projectDetailFragment);
    void inject(SponsorSelectProjFragment sponsorSelectProjFragment);

    void inject(CustomToolbar customToolbar);
    void inject(CustomBottomBar customBottomBar);
    void inject(CustomMenu customMenu);

    void inject(ForgotPassDialog forgotPassDialog);

}
