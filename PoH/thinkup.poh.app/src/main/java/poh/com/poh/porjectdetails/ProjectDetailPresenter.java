package poh.com.poh.porjectdetails;

import com.poh.thinkup.thinkuppohmodel.FullProject;
import com.poh.thinkup.thinkuppohmodel.Project;
import com.poh.thinkup.thinkuppohserivces.lib.ServiceError;
import com.poh.thinkup.thinkuppohserivces.projects.media.MediaCallback;
import com.poh.thinkup.thinkuppohserivces.projects.media.MediaRequest;
import com.poh.thinkup.thinkuppohserivces.projects.media.MediaResult;
import com.poh.thinkup.thinkuppohserivces.projects.media.MediaService;
import com.poh.thinkup.thinkuppohserivces.projects.profile.ProjectProfileCallback;
import com.poh.thinkup.thinkuppohserivces.projects.profile.ProjectProfileRequest;
import com.poh.thinkup.thinkuppohserivces.projects.profile.ProjectProfileResult;
import com.poh.thinkup.thinkuppohserivces.projects.profile.ProjectProfileService;

import java.util.Random;

import javax.inject.Inject;

import poh.com.poh.base.Presenter;
import poh.com.poh.utils.ShareManager;

public class ProjectDetailPresenter extends Presenter<ProjectDetailContract.View>
        implements ProjectDetailContract.Presenter<ProjectDetailContract.View> {

    private FullProject fullProject;

    @Inject
    public ProjectDetailPresenter() {
    }

    @Override
    public void init(Project project) {
        ProjectProfileService service = new ProjectProfileService();
        service.execute(new ProjectProfileRequest(project.getProjectID()), new ProjectProfileCallback() {
            @Override
            public void onProfileSuccess(ProjectProfileResult.Expose result) {
                fullProject = result.getProject();
                loadProject();
            }

            @Override
            public void onServiceFail(ServiceError error) {

            }
        });
    }

    @Override
    public void readMoreClicked() {
        view.loadFullDescription(fullProject.getProjectDescription());
    }

    @Override
    public void share(ShareManager.ShareTarget target) {
        view.share(fullProject, target);
    }

    private void loadProject() {
        view.loadInfo(fullProject.getTitle(), fullProject.getCategory());
        view.loadHeader(fullProject.getImage());
        view.loadCollaborators(fullProject.getUSDReceived(),
                (fullProject.getHumans() != null ? (int) fullProject.getHumans() : 0),
                (fullProject.getSponsors() != null ? (int) fullProject.getSponsors() : 0));
        view.loadDescription(fullProject.getProjectDescription());
        view.hideLoader();
        loadMedia();
    }

    private void loadMedia() {
        if (fullProject.getMedia() != null) {
            view.loadMedia(fullProject.getMedia().toString());
        } else {
            MediaService mediaService = new MediaService();
            mediaService.execute(new MediaRequest(fullProject.getProjectID()), new MediaCallback() {
                @Override
                public void onMediaSuccess(MediaResult.Expose result) {
                    if (result.getMedia() != null && result.getMedia().size() > 0) {
                        int random = new Random().nextInt(result.getMedia().size());
                        view.loadMedia(result.getMedia().get(random).getUrl());
                    }
                }

                @Override
                public void onServiceFail(ServiceError error) {

                }
            });
        }
    }
}
