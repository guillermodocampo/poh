package poh.com.poh.sponsors;

import com.poh.thinkup.thinkuppohmodel.Sponsor;
import com.poh.thinkup.thinkuppohserivces.lib.ServiceError;
import com.poh.thinkup.thinkuppohserivces.sponsors.all.AllSponsorsCallback;
import com.poh.thinkup.thinkuppohserivces.sponsors.all.AllSponsorsRequest;
import com.poh.thinkup.thinkuppohserivces.sponsors.all.AllSponsorsResult;
import com.poh.thinkup.thinkuppohserivces.sponsors.all.AllSponsorsService;

import java.sql.SQLOutput;

import javax.inject.Inject;

import poh.com.poh.base.Presenter;
import poh.com.poh.base.RxBus;

public class SponsorsPresenter extends Presenter<SponsorsContract.View> implements
        SponsorsContract.Presenter<SponsorsContract.View> {

    @Inject
    public SponsorsPresenter() {
        super();
    }

    @Override
    public void start(SponsorsContract.View view) {
        super.start(view);
        executeSponsors();
    }

    private void executeSponsors() {
        AllSponsorsService service = new AllSponsorsService();
        service.execute(new AllSponsorsRequest(), new AllSponsorsCallback() {
            @Override
            public void onServiceFail(ServiceError error) {
                System.out.println(error.toString());
            }

            @Override
            public void onAllSponsorsSuccess(AllSponsorsResult.Expose sponsorsResult) {
                view.loadSponsors(sponsorsResult.getSponsors());
            }
        });
    }

    @Override
    public void sponsorClicked(Sponsor sponsor) {
        RxBus.getInstance().send(sponsor);
    }
}
