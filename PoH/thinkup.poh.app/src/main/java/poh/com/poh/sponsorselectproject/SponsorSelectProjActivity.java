package poh.com.poh.sponsorselectproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.poh.thinkup.thinkuppohmodel.FullSponsor;
import com.poh.thinkup.thinkuppohmodel.Sponsor;

import butterknife.BindView;
import poh.com.poh.R;
import poh.com.poh.base.BaseMenuActivity;
import poh.com.poh.base.BasePresenter;
import poh.com.poh.main.CustomToolbar;
import poh.com.poh.utils.ExtraKeys;

public class SponsorSelectProjActivity extends BaseMenuActivity implements CustomToolbar.ToolbarListener {

    @BindView(R.id.custom_toolbar_control)
    protected CustomToolbar customToolbar;

    private FullSponsor fullSponsor;
    private Sponsor sponsor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customToolbar.setTitle(R.string.sponsor_detail_back_toolbar);
        customToolbar.setColor(2,2);

        if (savedInstanceState != null) {
            sponsor = (Sponsor) savedInstanceState.getSerializable(ExtraKeys.SPONSOR_KEY);
            fullSponsor = (FullSponsor) savedInstanceState.getSerializable(ExtraKeys.FULL_SPONSOR_KEY);
        } else {
            sponsor = (Sponsor) getIntent().getSerializableExtra(ExtraKeys.SPONSOR_KEY);
            fullSponsor = (FullSponsor) getIntent().getSerializableExtra(ExtraKeys.FULL_SPONSOR_KEY);
        }

        customToolbar.setNavigationClick(this);
        customToolbar.inflateFragment(SponsorSelectProjFragment.newInstance(sponsor, fullSponsor));
    }

    public static Intent getIntent(Activity source, Sponsor sponsor, FullSponsor fullSponsor) {
        Intent intent = new Intent(source, SponsorSelectProjActivity.class);
        intent.putExtra(ExtraKeys.SPONSOR_KEY, sponsor);
        intent.putExtra(ExtraKeys.FULL_SPONSOR_KEY, fullSponsor);
        return intent;
    }

    @Override
    public void initializeInjector() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    public void onToolbarItemClick(int id) {
        super.onBackPressed();
    }

    @Override
    public void onToolbarItemClick(int id, int from, int to) {

    }
}
