package poh.com.poh.projects;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.poh.thinkup.thinkuppohmodel.Category;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPFragment;
import poh.com.poh.erroremptyview.EmptyView;
import poh.com.poh.projects.adapters.CategoriesAdapter;
import poh.com.poh.projects.adapters.CategoryClick;

public class ProjectsFragment extends BaseMVPFragment implements ProjectsContract.View, CategoryClick {

    @BindView(R.id.recycler_categories)
    RecyclerView categoriesRecyclerView;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.main_layout)
    ConstraintLayout layout;
    @BindView(R.id.categories_container)
    LinearLayout categoriesLinearLayout;

    @Inject
    CategoriesAdapter categoriesAdapter;
    @Inject
    ProjectsPresenter presenter;

    public static ProjectsFragment newInstance() {
        ProjectsFragment fragment = new ProjectsFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.projects_fragment, container, false);
        ButterKnife.bind(this, root);
        getPresenter().start(this);

        return root;
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @Override
    protected ProjectsContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void loadCategories(List<Category> categories) {
        categoriesLinearLayout.setVisibility(View.VISIBLE);
        categoriesAdapter.init(categories, this);
        categoriesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        categoriesRecyclerView.setAdapter(categoriesAdapter);
        animationView.setVisibility(View.GONE);
    }

    @Override
    public void onCategoryClick(Category category) {
        getPresenter().categoryClicked(category);
    }

    @Override
    public void loadErrorView() {
        categoriesLinearLayout.setVisibility(View.GONE);
        animationView.setVisibility(View.GONE);
        new EmptyView(getContext(), layout)
                .icon(R.drawable.ic_warning)
                .title(R.string.error_title)
                .description(R.string.error_message)
                .show();
    }
}
