package poh.com.poh.sponsorselectproject.adapter;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.poh.thinkup.thinkuppohmodel.Project;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import poh.com.poh.R;

public class AllProjectsAdapter extends RecyclerView.Adapter<AllProjectsAdapter.AllProjectViewHolder> {

    private List<Project> projects;
    private SelectedProjectClick selectedProjectClick;

    @Inject
    public AllProjectsAdapter() {

    }

    public void init(List<Project> projectList, SelectedProjectClick projectClick){
        this.projects = projectList;
        this.selectedProjectClick = projectClick;
    }

    @NonNull
    @Override
    public AllProjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_project_all, parent, false);
        return new AllProjectViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final AllProjectViewHolder holder, int position) {
        final Project project = projects.get(position);
        holder.bind(projects.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.changeRBImage();
                selectedProjectClick.onSelectedProjectClick(project);
            }
        });
    }

    @Override
    public int getItemCount() {
        return projects != null ? projects.size() : 0;
    }

    public final class AllProjectViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_project_name)
        TextView textView;
        @BindView(R.id.image_view_category)
        ImageView imageView;
        @BindView(R.id.image_view_radio_btn)
        ImageView imageViewRB;

        private boolean isChecked = false;
        private String title = "";

        public String getTitle() {
            return title;
        }

        public AllProjectViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Project project) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedProjectClick.onSelectedProjectClick(project);
                }
            });
            textView.setText(project.getTitle());
            title = project.getTitle();
        }

        public void changeRBImage() {
            if(!isChecked){
                isChecked = true;
                imageViewRB.setBackground(ContextCompat.getDrawable(
                        imageViewRB.getContext(), R.drawable.ic_oval_selected));
            }
            else{
                isChecked = false;
                imageViewRB.setBackground(ContextCompat.getDrawable(
                        imageViewRB.getContext(), R.drawable.ic_oval));
            }
        }
    }

}
