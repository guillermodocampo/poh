package poh.com.poh.utils;

import android.content.Context;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;

import javax.inject.Inject;

import poh.com.poh.R;

public class SpannableBuilder {

    private Context context;

    @Inject
    public SpannableBuilder(Context context) {
        this.context = context;
    }

    public SpannableString createSpannableAcievements(int advices, int sponsors, String time, double dollars, int projects) {
        String totalUSD = DoubleFormatter.getDoubleWithTwoDecimalsString(dollars);
        int lengthUSD = 4 + totalUSD.length();
        int text1Length = 6;
        int text2Length = 11;
        int text3Length = 26;
        int text4Length = 19;
        int text5Length = 11;
        int text6Length = 11;
        int lengthAdvices = String.valueOf(advices).length();
        int lengthTime = String.valueOf(time).length();
        int lengthSponsors = String.valueOf(sponsors).length();
        int lengthProjects = String.valueOf(projects).length();

        SpannableString textSpannable = new SpannableString(context.getString(R.string.human_achievements_text,
                advices, sponsors, time, totalUSD, projects));

        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), 0,
                text1Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.collaborators_profile), text1Length,
                text1Length + lengthAdvices, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), text1Length + lengthAdvices,
                text1Length + lengthAdvices + text2Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.collaborators_profile), text1Length + lengthAdvices + text2Length,
                text1Length + lengthAdvices + text2Length + lengthSponsors, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), text1Length + lengthAdvices + text2Length + lengthSponsors,
                text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.collaborators_profile), text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length,
                text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime,
                text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime + text4Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.collaborators_profile), text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime + text4Length,
                text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime + text4Length + lengthUSD, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime + text4Length + lengthUSD,
                text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime + text4Length + lengthUSD + text5Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.collaborators_profile), text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime + text4Length + lengthUSD + text5Length,
                text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime + text4Length + lengthUSD + text5Length + lengthProjects, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime + text4Length + lengthUSD + text5Length + lengthProjects,
                text1Length + lengthAdvices + text2Length + lengthSponsors + text3Length + lengthTime + text4Length + lengthUSD + text5Length + lengthProjects + text6Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return textSpannable;
    }

    public SpannableString createSpannableCollaboration(double totalCollaboration, int humans, int sponsors) {
        String totalUSD = DoubleFormatter.getDoubleWithTwoDecimalsString(totalCollaboration);
        int lengthUSD = 4 + totalUSD.length();
        int text1Length = 21;
        int lengthHumans = String.valueOf(humans).length();
        int text2Length = 11;
        int lengthSponsors = String.valueOf(sponsors).length();
        int text3Length = 9;

        SpannableString textSpannable = new SpannableString(context.getString(R.string.project_detail_collaboration,
                totalUSD, humans, sponsors));

        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.usd_profile), 0,
                lengthUSD, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), lengthUSD,
                lengthUSD + text1Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.collaborators_profile), lengthUSD + text1Length,
                lengthUSD + text1Length + lengthHumans, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), lengthUSD + text1Length + lengthHumans,
                lengthUSD + text1Length + lengthHumans + text2Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.collaborators_profile), lengthUSD + text1Length + lengthHumans + text2Length,
                lengthUSD + text1Length + lengthHumans + text2Length + lengthSponsors, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), lengthUSD + text1Length + lengthHumans + text2Length + lengthSponsors,
                lengthUSD + text1Length + lengthHumans + text2Length + lengthSponsors + text3Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return textSpannable;
    }

    public SpannableString createSpannableCollaborationSponsor(double totalDonated, int projects, int humans) {
        String totalUSD = DoubleFormatter.getDoubleWithTwoDecimalsString(totalDonated);
        int lengthUSD = 4 + totalUSD.length();
        int text1Length = 11;
        int lengthHumans = String.valueOf(projects).length();
        int text2Length = 15;
        int lengthSponsors = String.valueOf(humans).length();
        int text3Length = 8;

        SpannableString textSpannable = new SpannableString(context.getString(R.string.sponsor_awards_detail,
                totalUSD, projects, humans));

        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.usd_profile_sponsor), 0,
                lengthUSD, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), lengthUSD,
                lengthUSD + text1Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.collaborators_profile), lengthUSD + text1Length,
                lengthUSD + text1Length + lengthHumans, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), lengthUSD + text1Length + lengthHumans,
                lengthUSD + text1Length + lengthHumans + text2Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.collaborators_profile), lengthUSD + text1Length + lengthHumans + text2Length,
                lengthUSD + text1Length + lengthHumans + text2Length + lengthSponsors, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.common_text), lengthUSD + text1Length + lengthHumans + text2Length + lengthSponsors,
                lengthUSD + text1Length + lengthHumans + text2Length + lengthSponsors + text3Length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return textSpannable;
    }

    public SpannableString createSpannableDescription(String html) {
        String finalString = stripHtml(html);
        int endLength = context.getString(R.string.project_detail_read_more).length();
        SpannableString textSpannable = new SpannableString(finalString);

        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.art_text), 0,
                finalString.length() - endLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.art_end_text), finalString.length() - endLength,
                finalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return textSpannable;
    }

    public SpannableString createSpannableFullDescription(String html) {
        String finalString = fromHTML(html);
        SpannableString textSpannable = new SpannableString(finalString);

        textSpannable.setSpan(new TextAppearanceSpan(context, R.style.art_text), 0,
                finalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return textSpannable;
    }

    private String stripHtml(String html) {
        html = replaceH1(html);
        html = replaceImg(html);

        html = fromHTML(html);
        return (html.length() > 150
                ? html.substring(0, 149)
                : html) + context.getString(R.string.project_detail_read_more);
    }

    private String fromHTML(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString().trim();
        } else {
            return Html.fromHtml(html).toString().trim();
        }
    }

    private String replaceH1(String html) {
        int start = 0;
        int end = 0;
        while (html.indexOf("<h1>", start) != -1) {
            start = html.indexOf("<h1>", start);
            end = html.indexOf("</h1>", start);
            html = html.replace(html.substring(start, end), "");
        }
        return html;
    }

    private String replaceImg(String html) {
        int start = 0;
        int end = 0;
        while (html.indexOf("<img", start) != -1) {
            start = html.indexOf("<img", start);
            end = html.indexOf(">", start);
            html = html.replace(html.substring(start, end), "");
        }
        return html;
    }
}
