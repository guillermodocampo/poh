package poh.com.poh.utils;

import java.util.Locale;

public class DoubleFormatter {

    private static final String DECIMAL_FORMAT = "%.2f";
    private static final String INTEGER_FORMAT = "%.0f";

    private static boolean hasDecimalComponents(double value) {
        return value % 1 != 0;
    }

    public static String getDecimalFormatString(double value) {
        if (hasDecimalComponents(value)) {
            return DECIMAL_FORMAT;
        }
        return INTEGER_FORMAT;
    }

    public static double getDoubleWithTwoDecimals(final double originalValue) {
        return Math.round(originalValue * 100.0) / 100.0;
    }

    public static String getDoubleWithTwoDecimalsString(final double originalDouble) {
        return String.format(Locale.getDefault(), getDecimalFormatString(originalDouble), originalDouble);
    }
}
