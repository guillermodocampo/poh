package poh.com.poh.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import androidx.core.content.FileProvider;
import android.widget.Toast;

import com.poh.thinkup.thinkuppohmodel.FullProject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

public class ShareManager {

    private static final String URL_FORMAT = "https://www.projectsofhumanity.org/Projects/%s";
    private static final String ERROR_FORMAT = "No hemos encontrado la aplicacion %s en tu dispositivo.";

    private Context context;
    private ImageLoader imageLoader;

    @Inject
    public ShareManager(Context context, ImageLoader imageLoader) {
        this.context = context;
        this.imageLoader = imageLoader;
    }

    public void shareProject(FullProject project, ShareTarget target) {
        switch (target) {
            case INSTAGRAM:
                onShareImage(project, target);
                break;
            default:
                onShare(project, target);
        }
    }

    private void onShare(FullProject project, ShareTarget target) {
        if (!share(project, target)) {
            Toast.makeText(context, String.format(ERROR_FORMAT, target.errorMessage), Toast.LENGTH_LONG).show();
        }
    }

    private void onShareImage(final FullProject project, final ShareTarget target) {
        imageLoader.load(project.getImage())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        share(bitmap);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        Toast.makeText(context, String.format(ERROR_FORMAT, target.errorMessage), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
    }

    private boolean share(FullProject project, ShareTarget target) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType(target.mimeType);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, project.getTitle());
        shareIntent.putExtra(Intent.EXTRA_TEXT, String.format(URL_FORMAT, project.getProjectID()));

        final PackageManager pm = context.getPackageManager();
        final List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
        for (final ResolveInfo app : activityList) {
            if (target.activityInfo.equals(app.activityInfo.name)) {
                final ActivityInfo activity = app.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                shareIntent.setComponent(name);
                context.startActivity(shareIntent);
                return true;
            }
        }
        return false;
    }

    private void share(Bitmap bitmap) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType(ShareTarget.INSTAGRAM.mimeType);
        shareIntent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));

        final PackageManager pm = context.getPackageManager();
        final List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
        for (final ResolveInfo app : activityList) {
            if (ShareTarget.INSTAGRAM.activityInfo.equals(app.activityInfo.name)) {
                final ActivityInfo activity = app.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                shareIntent.setComponent(name);
                context.startActivity(shareIntent);
                return;
            }
        }
        Toast.makeText(context, String.format(ERROR_FORMAT, ShareTarget.INSTAGRAM.errorMessage), Toast.LENGTH_LONG).show();
    }

    private Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".utils.provider", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public enum ShareTarget {
        INSTAGRAM("com.instagram.share.handleractivity.ShareHandlerActivity", "Instagram", "image/*"),
        TWITTER("com.twitter.composer.ComposerActivity", "Twitter", "text/plain"),
        FACEBOOK("com.facebook.composer.shareintent.ImplicitShareIntentHandlerDefaultAlias", "Facebook", "text/plain"),
        MAIL("com.google.android.gm.ComposeActivityGmailExternal", "Gmail", "text/plain");

        String activityInfo;
        String errorMessage;
        String mimeType;

        ShareTarget(String activityInfo, String errorMessage, String mimeType) {
            this.activityInfo = activityInfo;
            this.errorMessage = errorMessage;
            this.mimeType = mimeType;
        }
    }
}
