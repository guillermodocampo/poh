package poh.com.poh.sponsorselectproject.adapter;

import com.poh.thinkup.thinkuppohmodel.Project;

public interface SelectedProjectClick {
    void onSelectedProjectClick(Project project);
}
