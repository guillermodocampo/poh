package poh.com.poh.projects;

import com.poh.thinkup.thinkuppohmodel.Category;
import com.poh.thinkup.thinkuppohserivces.categories.CategoriesCallback;
import com.poh.thinkup.thinkuppohserivces.categories.CategoriesRequest;
import com.poh.thinkup.thinkuppohserivces.categories.CategoriesResult;
import com.poh.thinkup.thinkuppohserivces.categories.CategoriesService;
import com.poh.thinkup.thinkuppohserivces.lib.ServiceError;

import javax.inject.Inject;

import poh.com.poh.base.Presenter;
import poh.com.poh.base.RxBus;

public class ProjectsPresenter extends Presenter<ProjectsContract.View> implements ProjectsContract.Presenter<ProjectsContract.View> {

    @Inject
    public ProjectsPresenter() {
        super();
    }

    @Override
    public void start(ProjectsContract.View view) {
        super.start(view);
        executeCategories();
    }

    private void executeCategories() {
        CategoriesService service = new CategoriesService();
        service.execute(new CategoriesRequest(), new CategoriesCallback() {
            @Override
            public void onCategoriesSuccess(CategoriesResult.Expose result) {
                view.loadCategories(result.getCategories());
            }

            @Override
            public void onServiceFail(ServiceError error) {
                System.out.print(error);
                view.loadErrorView();
            }
        });
    }

    @Override
    public void categoryClicked(Category category) {
        RxBus.getInstance().send(category);
    }
}
