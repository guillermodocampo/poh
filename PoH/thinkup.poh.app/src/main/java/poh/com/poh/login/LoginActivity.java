package poh.com.poh.login;

import android.app.Activity;
import android.content.Intent;
import com.google.android.material.textfield.TextInputLayout;
import android.os.Bundle;
import androidx.core.content.res.ResourcesCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPActivity;
import poh.com.poh.forgot_pass.ForgotPass;
import poh.com.poh.forgot_pass.ForgotPassDialog;
import poh.com.poh.main.MainActivity;
import poh.com.poh.signup.SignUpActivity;

public class LoginActivity extends BaseMVPActivity implements LoginContract.View, ForgotPass {

    private ForgotPassDialog dialog;

    @BindView(R.id.textViewRestore) protected TextView textViewRestaurar;
    @BindView(R.id.textViewSignUp) protected TextView textViewRegistrarme;

    @BindView(R.id.emailWrapper) protected TextInputLayout emailWrapper;
    @BindView(R.id.passWrapper) protected TextInputLayout passWrapper;
    @BindView(R.id.editTextEmail) protected EditText editTextEmail;
    @BindView(R.id.editTextPass) protected EditText editTextPass;

    @BindView(R.id.textViewLogin) protected TextView textViewLogin;

    @Inject
    LoginPresenter<LoginContract.View> presenter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setFonts();
        presenter.start(this);
    }

    /* Se setean las fuentes de los hint de los TextInputLayout.
     * Se hace por codigo ya que por styles no se pueden poner 2 fuentes distintas
      * para el edit text y para el hint.*/
    private void setFonts() {
        emailWrapper.setTypeface(ResourcesCompat.getFont(this, R.font.futura_bold));
        passWrapper.setTypeface(ResourcesCompat.getFont(this, R.font.futura_bold));
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @Override
    protected LoginContract.Presenter getPresenter() {
        return presenter;
    }

    @OnClick({R.id.textViewSignUp})
    public void goTosignUp(View view){
        Intent intent = new Intent(view.getContext(), SignUpActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.textViewRestore)
    public void restore(View view){
        dialog = new ForgotPassDialog();
        dialog.show(getFragmentManager(), "ForgotPassDialog");
    }

    @OnClick(R.id.textViewLogin)
    public void login(View view){

        String email = "", password = "";
        if(emailWrapper.getEditText() != null && passWrapper.getEditText() != null){
            email = emailWrapper.getEditText().getText().toString();
            password = passWrapper.getEditText().getText().toString();
        }
        presenter.logIn(email, password);
    }

    @OnFocusChange({R.id.editTextEmail, R.id.editTextPass})
    public void hideBoard(View view){

        if(!view.hasFocus()){
            hideKeyboard(view);
        }
        else{
            showSoftKeyboard(view);
        }
    }

    /* Change text, color and enables bottom button. */
    @OnTextChanged({R.id.editTextEmail, R.id.editTextPass})
    public void changeTextLogin(){
        if (!editTextEmail.getText().toString().equals("")
                && !editTextPass.getText().toString().equals("")){
            textViewLogin.setText(R.string.login_enter);
            textViewLogin.setEnabled(true);
            textViewLogin.setTextColor(getResources().getColor(R.color.main_projects));
        }
        else{
            textViewLogin.setText(R.string.login_insert);
            textViewLogin.setEnabled(false);
            textViewLogin.setTextColor(getResources().getColor(R.color.main_projects_30));
        }
    }

    @Override
    public void showEmailError() {
        emailWrapper.setError(getString(R.string.error_email));
    }

    @Override
    public void removeEmailError() {
        emailWrapper.setErrorEnabled(false);
    }

    @Override
    public void showPassError() {
        passWrapper.setError(getString(R.string.error_pass));
    }

    @Override
    public void removePassError() {
        passWrapper.setErrorEnabled(false);
    }

    @Override
    public void showSendEmailError() {
        dialog.showEmailError(getString(R.string.error_email));
    }

    @Override
    public void removeSendEmailError() {
        dialog.removeEmailError();
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        view.requestFocus();
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(view, 0);
        }
    }

    @Override
    public void sendEmail(String email) {
        presenter.sendEmail(email);
    }
}

