package poh.com.poh.base;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.jaeger.library.StatusBarUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import poh.com.poh.R;
import poh.com.poh.main.CustomToolbar;

public abstract class BaseMenuActivity extends BaseMVPActivity {

    @BindView(R.id.custom_toolbar_control)
    protected CustomToolbar customToolbar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.default_toolbar_activity);
        ButterKnife.bind(this);

        StatusBarUtil.setTransparent(this);

        customToolbar.setUtils(getSupportFragmentManager());
        customToolbar.setResourceLayout(R.layout.default_frame_layout);
    }
}
