package poh.com.poh.login;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;

public interface LoginContract {

    interface Presenter<T extends BaseView> extends BasePresenter<T> {
        void logIn(String email, String pass);
        void sendEmail(String email);
    }

    interface View extends BaseView {
        void showEmailError();
        void removeEmailError();
        void showPassError();
        void removePassError();
        void showSendEmailError();
        void removeSendEmailError();

        void goToMain();
    }
}
