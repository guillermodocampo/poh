package poh.com.poh.erroremptyview

import android.graphics.drawable.Drawable

class EmptyViewModel(val drawable: Drawable?, val title: String, var description: String, val action: String)