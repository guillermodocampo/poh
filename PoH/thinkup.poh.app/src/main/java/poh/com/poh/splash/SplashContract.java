package poh.com.poh.splash;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;

public interface SplashContract {

    interface Presenter<T extends BaseView> extends BasePresenter<T> {
        void checkUserLogged();
    }

    interface View extends BaseView {
        void goToLogin();
        void goToMain();
    }

}
