package poh.com.poh.sponsors;

import com.poh.thinkup.thinkuppohmodel.Sponsor;

import java.util.List;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;

public interface SponsorsContract {
    interface Presenter<T extends BaseView> extends BasePresenter<T> {
        void sponsorClicked(Sponsor sponsor);
    }

    interface View extends BaseView {
        void loadSponsors(List<Sponsor> sponsors);
    }
}
