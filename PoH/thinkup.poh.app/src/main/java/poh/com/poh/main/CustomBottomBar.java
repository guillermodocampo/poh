package poh.com.poh.main;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import poh.com.poh.PohApplication;
import poh.com.poh.R;
import poh.com.poh.dagger.components.MainComponent;
import poh.com.poh.utils.Constants;
import poh.com.poh.utils.UIUtils;

public class CustomBottomBar extends ConstraintLayout implements TabLayout.OnTabSelectedListener {

    private int paddingSelected;
    private int previousPosition;
    private TabSelectedChange listener;

    @Inject
    UIUtils uiUtils;

    @BindView(R.id.image_projects)
    ImageView projectImageView;
    @BindView(R.id.image_me)
    ImageView humanImageView;
    @BindView(R.id.image_sponsors)
    ImageView sponsorImageView;
    @BindView(R.id.tab_menu)
    TabLayout menuTabLayout;

    public CustomBottomBar(Context context) {
        super(context);
        init();
    }

    public CustomBottomBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBottomBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        getDaggerComponent().inject(this);
        View root = inflate(getContext(), R.layout.custom_bottom_bar, this);
        ButterKnife.bind(this, root);

        paddingSelected = getContext().getResources().getDimensionPixelSize(R.dimen.bottom_bar_padding_selected);
        menuTabLayout.addOnTabSelectedListener(this);
    }

    public void setClickListener(TabSelectedChange listener) {
        this.listener = listener;
    }

    public void setColor(int to) {
        menuTabLayout.getTabAt(to).select();
    }

    public void setColor(int from, int to) {
        uiUtils.animateBackgroundPosition(menuTabLayout, from, to);
    }

    private void onTabSelected(int position) {
        switch (position) {
            case Constants.PROJECTS_INDEX:
                menuTabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.unselected_project_tabs),
                        ContextCompat.getColor(getContext(), R.color.white));
                animatePaddingIn(projectImageView);
                listener.onSelectTab(R.id.tab_projects, previousPosition, Constants.PROJECTS_INDEX);
                break;
            case Constants.HUMANS_INDEX:
                menuTabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.unselected_human_tabs),
                        ContextCompat.getColor(getContext(), R.color.white));
                animatePaddingIn(humanImageView);
                listener.onSelectTab(R.id.tab_me, previousPosition, Constants.HUMANS_INDEX);
                break;
            case Constants.SPONSORS_INDEX:
                menuTabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.unselected_sponsor_tabs),
                        ContextCompat.getColor(getContext(), R.color.white));
                animatePaddingIn(sponsorImageView);
                listener.onSelectTab(R.id.tab_sponsors, previousPosition, Constants.SPONSORS_INDEX);
                break;
        }
    }

    private void onTabUnselected(int position) {
        switch (position) {
            case Constants.PROJECTS_INDEX:
                animatePaddingOut(projectImageView);
                break;
            case Constants.HUMANS_INDEX:
                animatePaddingOut(humanImageView);
                break;
            case Constants.SPONSORS_INDEX:
                animatePaddingOut(sponsorImageView);
                break;
        }
    }

    private void animatePaddingIn(final View view) {
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.setPadding(0, 0, 0, (int) (paddingSelected * interpolatedTime));
            }
        };
        a.setDuration(380); // in ms
        view.startAnimation(a);
    }

    private void animatePaddingOut(final View view) {
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.setPadding(0, 0, 0, 0);
            }
        };
        a.setDuration(280); // in ms
        view.startAnimation(a);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        onTabSelected(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        onTabUnselected(tab.getPosition());
        previousPosition = tab.getPosition();
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @OnClick(R.id.image_projects)
    public void ProjectImageClick() {
        menuTabLayout.getTabAt(Constants.PROJECTS_INDEX).select();
    }

    @OnClick(R.id.image_me)
    public void HumanImageClick() {
        menuTabLayout.getTabAt(Constants.HUMANS_INDEX).select();
    }

    @OnClick(R.id.image_sponsors)
    public void SponsorImageClick() {
        menuTabLayout.getTabAt(Constants.SPONSORS_INDEX).select();
    }

    private MainComponent getDaggerComponent() {
        return ((PohApplication) getContext().getApplicationContext()).getDaggerWrapper().getMainComponent();
    }

    public interface TabSelectedChange {
        void onSelectTab(int id, int fromPosition, int toPosition);
    }
}
