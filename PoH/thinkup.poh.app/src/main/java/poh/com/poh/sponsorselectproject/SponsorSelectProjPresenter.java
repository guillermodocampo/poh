package poh.com.poh.sponsorselectproject;

import com.poh.thinkup.thinkuppohmodel.FullSponsor;
import com.poh.thinkup.thinkuppohserivces.lib.ServiceError;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectsResult;
import com.poh.thinkup.thinkuppohserivces.projects.all.AllProjectRequest;
import com.poh.thinkup.thinkuppohserivces.projects.all.AllProjectService;
import com.poh.thinkup.thinkuppohserivces.projects.all.AllProjectsCallback;
import com.poh.thinkup.thinkuppohserivces.sponsors.all.AllSponsorsService;

import javax.inject.Inject;

import poh.com.poh.base.Presenter;

public class SponsorSelectProjPresenter extends Presenter<SponsorSelectProjContract.View>
        implements SponsorSelectProjContract.Presenter<SponsorSelectProjContract.View> {

    private FullSponsor fullSponsor;

    @Inject
    public SponsorSelectProjPresenter() {
    }

    @Override
    public void init(FullSponsor fullSponsor) {
        this.fullSponsor = fullSponsor;
        loadSponsor();
        AllProjectService service = new AllProjectService();
        service.execute(new AllProjectRequest(), new AllProjectsCallback() {
            @Override
            public void onAllProjectsSuccess(ProjectsResult.Expose projectsResult) {
                view.loadProjects(projectsResult.getProjects());
            }

            @Override
            public void onServiceFail(ServiceError error) {

            }
        });
    }

    @Override
    public void loadSponsor() {
        view.loadInfo(fullSponsor.getFirstName(), fullSponsor.getType());
        view.loadHeader(fullSponsor.getAvatar());
        view.showHeader();
        view.hideLoader();
    }
}
