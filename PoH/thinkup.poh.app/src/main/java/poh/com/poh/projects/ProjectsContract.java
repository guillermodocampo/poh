package poh.com.poh.projects;

import com.poh.thinkup.thinkuppohmodel.Category;

import java.util.List;

import poh.com.poh.base.BasePresenter;
import poh.com.poh.base.BaseView;

public interface ProjectsContract {

    interface Presenter<T extends BaseView> extends BasePresenter<T>  {
        void categoryClicked(Category category);
    }

    interface View extends BaseView {
        void loadCategories(List<Category> categories);
        void loadErrorView();
    }
}
