package poh.com.poh.projects.adapters;

import com.poh.thinkup.thinkuppohmodel.Category;

public interface CategoryClick {
    void onCategoryClick(Category category);
}
