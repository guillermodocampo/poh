package poh.com.poh.projects.bycategory;

import com.poh.thinkup.thinkuppohmodel.Category;
import com.poh.thinkup.thinkuppohserivces.lib.ServiceError;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectsResult;
import com.poh.thinkup.thinkuppohserivces.projects.bycategory.ProjectByCategoryService;
import com.poh.thinkup.thinkuppohserivces.projects.bycategory.ProjectByCategoryCallback;
import com.poh.thinkup.thinkuppohserivces.projects.bycategory.ProjectByCategoryRequest;

import javax.inject.Inject;

import poh.com.poh.base.Presenter;

public class CategoryProjectsPresenter extends Presenter<CategoryProjectsContract.View> implements
        CategoryProjectsContract.Presenter<CategoryProjectsContract.View> {

    @Inject
    public CategoryProjectsPresenter() {
    }

    @Override
    public void init(Category category) {
        view.loadTitle(category.getName());
        executeByCategory(category);
    }

    private void executeByCategory(Category category) {
        ProjectByCategoryService service = new ProjectByCategoryService();
        service.execute(new ProjectByCategoryRequest(category.getName()), new ProjectByCategoryCallback() {
            @Override
            public void onByCategorySuccess(ProjectsResult.Expose projectsResult) {
                view.loadProjects(projectsResult.getProjects());
            }

            @Override
            public void onServiceFail(ServiceError error) {
                System.out.print(error);
            }
        });
    }
}
