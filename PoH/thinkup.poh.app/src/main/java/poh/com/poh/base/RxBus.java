package poh.com.poh.base;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBus {

    private static RxBus instance;
    private final PublishSubject<Object> bus = PublishSubject.create();

    private RxBus() {
    }

    public static RxBus getInstance() {
        return instance == null ? instance = new RxBus() : instance;
    }

    public void send(Object o) {
        bus.onNext(o);
    }

    public Observable<Object> toObserverable() {
        return bus;
    }
}
