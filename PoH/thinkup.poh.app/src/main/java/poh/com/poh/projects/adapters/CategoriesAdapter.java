package poh.com.poh.projects.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.poh.thinkup.thinkuppohmodel.Category;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import poh.com.poh.R;
import poh.com.poh.utils.ImageLoader;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder> {

    private ImageLoader imageLoader;
    private List<Category> categories;
    private CategoryClick categoryClick;

    @Inject
    public CategoriesAdapter(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    public void init(List<Category> categories, CategoryClick categoryClick) {
        this.categories = categories;
        this.categoryClick = categoryClick;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        final Category category = categories.get(position);
        holder.bind(category);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryClick.onCategoryClick(category);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories != null ? categories.size() : 0;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_category_image)
        ImageView imageView;
        @BindView(R.id.item_category_text)
        TextView textView;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Category category) {
            textView.setText(category.getName());
            imageLoader.load(category.getName())
                    .error(R.drawable.cat_science)
                    .placeholder(R.drawable.cat_science)
                    .into(imageView);
        }

    }
}
