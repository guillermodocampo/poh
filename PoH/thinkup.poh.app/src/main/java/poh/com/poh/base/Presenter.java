package poh.com.poh.base;

import com.poh.thinkup.thinkuppohserivces.base.ServiceQueue;

/**
 * Created by nicolas.castro on 27/11/2017.
 */

public abstract class Presenter<T extends BaseView> implements BasePresenter<T> {

    protected T view;

    protected Presenter() {
    }

    @Override
    public void start(T view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        ServiceQueue.dispose();
    }

    protected void setView(T view) {
        this.view = view;
    }

    protected T getView() {
        return view;
    }
}
