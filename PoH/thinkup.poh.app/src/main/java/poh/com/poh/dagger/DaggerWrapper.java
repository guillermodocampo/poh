package poh.com.poh.dagger;

import poh.com.poh.PohApplication;
import poh.com.poh.dagger.components.DaggerMainComponent;
import poh.com.poh.dagger.components.MainComponent;
import poh.com.poh.dagger.modules.MainModule;

public class DaggerWrapper {

    private MainComponent mainComponent;

    public void init(PohApplication application) {
        MainModule mainModule = new MainModule(application);

        mainComponent = DaggerMainComponent.builder()
                .mainModule(mainModule)
                .build();
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }
}
