package poh.com.poh.sponsorselectproject;

import android.os.Bundle;

import com.airbnb.lottie.LottieAnimationView;
import com.poh.thinkup.thinkuppohmodel.FullSponsor;
import com.poh.thinkup.thinkuppohmodel.Project;
import com.poh.thinkup.thinkuppohmodel.Sponsor;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPFragment;
import poh.com.poh.sponsorselectproject.adapter.AllProjectsAdapter;
import poh.com.poh.sponsorselectproject.adapter.SelectedProjectClick;
import poh.com.poh.utils.ExtraKeys;
import poh.com.poh.utils.ImageLoader;

public class SponsorSelectProjFragment extends BaseMVPFragment implements SponsorSelectProjContract.View, SelectedProjectClick {

    @BindView(R.id.category_name_sponsor)
    TextView categoryTextView;
    @BindView(R.id.sponsor_name)
    TextView nameTextView;
    @BindView(R.id.animation_view_projects)
    protected LottieAnimationView animationViewProjects;
    @BindView(R.id.animation_view_image)
    protected LottieAnimationView animationViewImage;
    @BindView(R.id.profile_header_image)
    ImageView headerImageView;
    @BindView(R.id.recycler_all_projects)
    RecyclerView allProjectsRecyclerView;
    @BindView(R.id.view_gradient)
    View viewGradient;
    @BindView(R.id.profile_favorite)
    ImageView profileFavorite;
    @BindView(R.id.click_project_to_donate)
    LinearLayout ll_donate_proj;
    @BindView(R.id.text_view_donate_project)
    TextView textView_donate_to_proj;

    @Inject
    SponsorSelectProjPresenter presenter;
    @Inject
    AllProjectsAdapter allProjectsAdapter;
    @Inject
    ImageLoader imageLoader;

    private boolean isSelected = false;

    public static SponsorSelectProjFragment newInstance(Sponsor sponsor, FullSponsor fullSponsor) {
        SponsorSelectProjFragment fragment = new SponsorSelectProjFragment();
        Bundle args = new Bundle();
        args.putSerializable(ExtraKeys.SPONSOR_KEY, sponsor);
        args.putSerializable(ExtraKeys.FULL_SPONSOR_KEY, fullSponsor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.sponsors_select_proj_fragment, container, false);
        ButterKnife.bind(this, root);
        getPresenter().start(this);
        getPresenter().init((FullSponsor) getArguments().getSerializable(ExtraKeys.FULL_SPONSOR_KEY));
        return root;
    }

    @Override
    protected SponsorSelectProjContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @OnClick(R.id.click_project_to_donate)
    public void watchAdvise(View view){
        if(isSelected){
            Toast.makeText(view.getContext(), "SE MIRA EL AVISO", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void hideLoader() {
        animationViewProjects.pauseAnimation();
        animationViewProjects.setVisibility(View.GONE);
        animationViewImage.pauseAnimation();
        animationViewImage.setVisibility(View.GONE);
    }

    @Override
    public void loadInfo(String sponsorName, String category) {
        categoryTextView.setText(category);
        nameTextView.setText(sponsorName);
    }

    @Override
    public void loadHeader(String url) {
        imageLoader.load(url).into(headerImageView);
    }

    @Override
    public void loadProjects(List<Project> projects) {
        allProjectsAdapter.init(projects, this);
        allProjectsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        allProjectsRecyclerView.setAdapter(allProjectsAdapter);
        animationViewProjects.setVisibility(View.GONE);
    }

    @Override
    public void showHeader() {
        viewGradient.setVisibility(View.VISIBLE);
        profileFavorite.setVisibility(View.VISIBLE);
        categoryTextView.setVisibility(View.VISIBLE);
        nameTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSelectedProjectClick(Project project) {

        Float alpha;
        if(!isSelected){
            isSelected = true;
            alpha = .4f;
            changeBottomButton(true, R.string.sponsor_donate, getResources().getColor(R.color.white), R.drawable.ic_donate_white);
        }
        else{
            isSelected = false;
            alpha = 1f;
            changeBottomButton(false, R.string.sponsor_select_project, getResources().getColor(R.color.white_30), 0);
        }

        enableDisableList(!isSelected, project.getTitle(), alpha);
    }

    private void changeBottomButton(boolean bool, int resourceText, int color, int end) {
        ll_donate_proj.setClickable(bool);
        textView_donate_to_proj.setText(resourceText);
        textView_donate_to_proj.setEnabled(bool);
        textView_donate_to_proj.setTextColor(color);
        textView_donate_to_proj.setCompoundDrawablesWithIntrinsicBounds(0, 0, end,0);
        textView_donate_to_proj.setCompoundDrawablePadding(getResources().getDimensionPixelOffset(R.dimen.padding_text_10dp));

    }


    private void enableDisableList(final boolean isSelected, final String projectTitle, final float alpha) {

        for (int i=0; i < allProjectsRecyclerView.getChildCount(); i++){
            final int position = i;
            allProjectsRecyclerView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    AllProjectsAdapter.AllProjectViewHolder holder = (AllProjectsAdapter.AllProjectViewHolder)
                            allProjectsRecyclerView.findViewHolderForAdapterPosition(position);
                    if (null != holder){
                        TextView title = holder.itemView.findViewById(R.id.text_view_project_name);
                        if(!projectTitle.equals(title.getText().toString())){
                            holder.itemView.setEnabled(isSelected);
                            holder.itemView.setAlpha(alpha);
                        }
                    }
                }
            },20);
        }
    }
}
