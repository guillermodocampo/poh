package poh.com.poh.main;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import poh.com.poh.PohApplication;
import poh.com.poh.R;
import poh.com.poh.dagger.components.MainComponent;
import poh.com.poh.utils.ActivityUtils;
import poh.com.poh.utils.Constants;
import poh.com.poh.utils.UIUtils;

public class CustomToolbar extends CoordinatorLayout implements MenuItem.OnMenuItemClickListener, CustomMenu.MenuListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.frame_container)
    FrameLayout frameLayout;
    @BindView(R.id.custom_toolbar_container)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.custom_menu_control)
    CustomMenu customMenu;

    @Inject
    ActivityUtils activityUtils;
    @Inject
    UIUtils uiUtils;

    private boolean hasReplaceableFragment = false;
    private int resourceLayout;
    private boolean hasMenuOpen = false;
    private int actualPosition;

    private ToolbarListener listener;

    public CustomToolbar(Context context) {
        super(context);
        init(null);
    }

    public CustomToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        getDaggerComponent().inject(this);

        if (attrs != null) {
            TypedArray attributes = getContext().getTheme().obtainStyledAttributes(
                    attrs, R.styleable.CustomToolbar, 0, 0);
            resourceLayout = attributes.getResourceId(R.styleable.CustomToolbar_customLayout, -1);
        }
        View view = inflate(getContext(), R.layout.custom_toolbar, this);

        ButterKnife.bind(this, view);

        inflateCustomLayout();
        inflateMenu();
    }

    private void inflateMenu() {
        toolbar.inflateMenu(R.menu.home_menu);
        MenuItem item = toolbar.getMenu().findItem(R.id.home_menu_item);
        item.setOnMenuItemClickListener(this);
        customMenu.setListener(this);
    }

    private void inflateCustomLayout() {
        if (hasCustomLayout())
            LayoutInflater.from(getContext()).inflate(resourceLayout, frameLayout, true);
    }

    public boolean hasReplaceableFragment() {
        return hasReplaceableFragment;
    }

    public void setHasReplaceableFragment(boolean hasReplaceableFragment) {
        this.hasReplaceableFragment = hasReplaceableFragment;
    }

    public void setResourceLayout(@LayoutRes int resourceLayout) {
        this.resourceLayout = resourceLayout;
        inflateCustomLayout();
    }

    public void setTitle(String title) {
        setArrowBack();
        toolbar.setTitle(title);
    }

    public void setTitle(@StringRes int title) {
        setArrowBack();
        toolbar.setTitle(title);
    }

    public void setNavigationClick(final ToolbarListener listener) {
        setClickListener(listener);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onToolbarItemClick(R.id.tab_projects);
                }
            }
        });
    }

    public void inflateFragment(Fragment fragment) {
        activityUtils.addFragment(fragment, R.id.fragment_container);
    }

    public void replaceFragment(Fragment fragment, int resId) {
        setArrowBack();
        toolbar.setTitle(resId);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onToolbarItemClick(R.id.tab_projects);
                }
            }
        });
        activityUtils.addFragmentNotChecked(fragment, R.id.fragment_container);
    }

    public void restoreMainToolbar() {
        hasReplaceableFragment = false;
        toolbar.setTitle(null);
        toolbar.setNavigationIcon(actualPosition == 0 ? R.drawable.poh_action_bar : R.drawable.poh_action_bar_dark);
        paintMenuIcon();
        customMenu.changeItemsColor(actualPosition);
        toolbar.setPadding(getResources().getDimensionPixelSize(R.dimen.padding_text_10dp), 0, 0, 0);
        toolbar.setNavigationOnClickListener(null);
    }

    private void paintMenuIcon() {
        MenuItem item = toolbar.getMenu().findItem(R.id.home_menu_item);
        item.getIcon().setColorFilter(actualPosition == 0 ? Color.WHITE : Color.BLACK, PorterDuff.Mode.SRC_IN);
    }

    private void setArrowBack() {
        hasReplaceableFragment = true;
        toolbar.setPadding(0, 0, 0, 0);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
    }

    private boolean hasCustomLayout() {
        return resourceLayout != -1;
    }

    public void setClickListener(ToolbarListener listener) {
        this.listener = listener;
    }

    public void setUtils(FragmentManager fragmentManager) {
        activityUtils.init(fragmentManager);
    }

    public void setColor(int fromPosition, int toPosition) {
        actualPosition = toPosition;
        if (hasMenuOpen) {
            customMenu.setVisible(hasMenuOpen);
            hasMenuOpen = !hasMenuOpen;
            animateIcon(toolbar.getMenu().findItem(R.id.home_menu_item), !hasMenuOpen);
        }
        uiUtils.animateBackgroundPosition(this, fromPosition, toPosition);
        uiUtils.animateBackgroundPosition(toolbar, fromPosition, toPosition);
    }

    private int getPosition(int id) {
        switch (id) {
            case R.id.menu_me:
                return Constants.HUMANS_INDEX;
            case R.id.menu_sponsors:
                return Constants.SPONSORS_INDEX;
            case R.id.menu_projects:
            default:
                return Constants.PROJECTS_INDEX;
        }
    }

    private void animateIcon(MenuItem item, boolean menuStatus) {
        Drawable newDrawable = !menuStatus
                ? ContextCompat.getDrawable(getContext(), R.drawable.ic_menu_animatable)
                : ContextCompat.getDrawable(getContext(), R.drawable.ic_menu_animatable_reversed);
        newDrawable.setColorFilter(actualPosition == 0 ? Color.WHITE : Color.BLACK, PorterDuff.Mode.SRC_IN);
        item.setIcon(newDrawable);
        AnimatedVectorDrawable drawable = (AnimatedVectorDrawable) item.getIcon();
        drawable.start();
    }

    private MainComponent getDaggerComponent() {
        return ((PohApplication) getContext().getApplicationContext()).getDaggerWrapper().getMainComponent();
    }

    @Override
    public boolean onMenuItemClick(final MenuItem item) {
        final boolean menuStatus = hasMenuOpen;
        animateIcon(item, menuStatus);

        uiUtils.animateBackground(toolbar, actualPosition, menuStatus);
        uiUtils.animateBackground(CustomToolbar.this, actualPosition, menuStatus);
        customMenu.setVisible(actualPosition, menuStatus);
        hasMenuOpen = !menuStatus;
        return true;
    }

    @Override
    public void onMenuItemClick(int id) {
        if (listener != null) {
            listener.onToolbarItemClick(id, actualPosition, getPosition(id));
        }
    }

    public interface ToolbarListener {
        void onToolbarItemClick(int id);

        void onToolbarItemClick(int id, int from, int to);
    }
}
