package poh.com.poh.human;

import com.poh.thinkup.thinkuppohmodel.Human;

import javax.inject.Inject;

import poh.com.poh.base.Presenter;

public class HumanPresenter extends Presenter<HumanContract.View> implements HumanContract.Presenter<HumanContract.View> {

    @Inject
    public HumanPresenter() {
    }


    @Override
    public void init() {
        //TODO: levantar userId from preferences & call service
        String userId = "nicoc_ale";
        view.loadHuman(new Human());
    }
}
