package poh.com.poh.porjectdetails;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.poh.thinkup.thinkuppohmodel.Project;

import butterknife.BindView;
import poh.com.poh.R;
import poh.com.poh.base.BaseMenuActivity;
import poh.com.poh.base.BasePresenter;
import poh.com.poh.main.CustomToolbar;
import poh.com.poh.utils.ExtraKeys;

public class ProjectDetailActivity extends BaseMenuActivity implements CustomToolbar.ToolbarListener {

    @BindView(R.id.custom_toolbar_control)
    protected CustomToolbar customToolbar;

    private Project project;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customToolbar.setTitle(R.string.project_detail_back_toolbar);

        if (savedInstanceState != null) {
            project = (Project) savedInstanceState.getSerializable(ExtraKeys.PROJECT_KEY);
        } else {
            project = (Project) getIntent().getSerializableExtra(ExtraKeys.PROJECT_KEY);
        }

        customToolbar.setNavigationClick(this);
        customToolbar.inflateFragment(ProjectDetailFragment.newInstance(project));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ExtraKeys.PROJECT_KEY, project);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void initializeInjector() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    public static Intent getIntent(Activity source, Project project) {
        Intent intent = new Intent(source, ProjectDetailActivity.class);
        intent.putExtra(ExtraKeys.PROJECT_KEY, project);
        return intent;
    }

    @Override
    public void onToolbarItemClick(int id) {
        super.onBackPressed();
    }

    @Override
    public void onToolbarItemClick(int id, int from, int to) {

    }
}
