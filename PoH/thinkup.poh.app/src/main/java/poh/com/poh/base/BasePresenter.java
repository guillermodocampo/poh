package poh.com.poh.base;

/**
 * Created by nicolas.castro on 10/10/2017.
 */

public interface BasePresenter<T extends BaseView> {
    void start(T view);
    void dropView();
}
