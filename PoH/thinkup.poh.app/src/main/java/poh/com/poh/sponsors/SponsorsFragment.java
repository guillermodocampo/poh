package poh.com.poh.sponsors;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airbnb.lottie.LottieAnimationView;
import com.poh.thinkup.thinkuppohmodel.Sponsor;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import poh.com.poh.R;
import poh.com.poh.base.BaseMVPFragment;
import poh.com.poh.sponsors.adapters.SponsorClick;
import poh.com.poh.sponsors.adapters.SponsorsAdapter;
import poh.com.poh.sponsorsdetails.SponsorDetailsActivity;

public class SponsorsFragment extends BaseMVPFragment implements SponsorsContract.View, SponsorClick {

    @BindView(R.id.recycler_sponsors)
    RecyclerView sponsorsRecyclerView;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;

    @Inject
    SponsorsAdapter sponsorsAdapter;
    @Inject
    SponsorsPresenter presenter;

    public static SponsorsFragment newInstance() {
        SponsorsFragment fragment = new SponsorsFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.sponsors_fragment, container, false);
        ButterKnife.bind(this, root);
        getPresenter().start(this);

        return root;
    }

    @Override
    protected SponsorsContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void initializeInjector() {
        getMainComponent().inject(this);
    }

    @Override
    public void loadSponsors(List<Sponsor> sponsors) {
        sponsorsAdapter.init(sponsors, this);
        sponsorsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        sponsorsRecyclerView.setAdapter(sponsorsAdapter);
        animationView.setVisibility(View.GONE);
    }

    @Override
    public void onSponsorClick(Sponsor sponsor) {
        Intent intent = SponsorDetailsActivity.getIntent(getActivity(), sponsor);
        startActivity(intent);
    }
}
