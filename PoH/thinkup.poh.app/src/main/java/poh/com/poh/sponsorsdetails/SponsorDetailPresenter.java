package poh.com.poh.sponsorsdetails;

import com.poh.thinkup.thinkuppohmodel.FullSponsor;
import com.poh.thinkup.thinkuppohmodel.Sponsor;
import com.poh.thinkup.thinkuppohserivces.lib.ServiceError;
import com.poh.thinkup.thinkuppohserivces.sponsors.profile.SponsorDataCallback;
import com.poh.thinkup.thinkuppohserivces.sponsors.profile.SponsorDataRequest;
import com.poh.thinkup.thinkuppohserivces.sponsors.profile.SponsorDataResult;
import com.poh.thinkup.thinkuppohserivces.sponsors.profile.SponsorDataService;

import javax.inject.Inject;

import poh.com.poh.base.Presenter;

public class SponsorDetailPresenter extends Presenter<SponsorDetailContract.View>
        implements SponsorDetailContract.Presenter<SponsorDetailContract.View> {

    private FullSponsor fullSponsor;
    private Sponsor sponsor;

    @Inject
    public SponsorDetailPresenter() {
    }

    public FullSponsor getFullSponsor() {
        return fullSponsor;
    }

    public Sponsor getSponsor() {
        return sponsor;
    }

    @Override
    public void init(Sponsor sponsor) {
        this.sponsor = sponsor;
        SponsorDataService service = new SponsorDataService();
        service.execute(new SponsorDataRequest(sponsor.getSponsorID()), new SponsorDataCallback() {
            @Override
            public void onSponsorDataSuccess(SponsorDataResult.Expose sponsorDataResult) {
                fullSponsor = sponsorDataResult.getSponsor();
                loadSponsor();
            }

            @Override
            public void onServiceFail(ServiceError error) {

            }
        });
    }

    @Override
    public void loadSponsor() {
        view.loadInfo(fullSponsor.getFirstName(), fullSponsor.getType());
        view.loadHeader(fullSponsor.getAvatar());
        view.loadCollaborators(sponsor.getUSDDonated(),
                (fullSponsor.getHumans() != null ? (int) fullSponsor.getHumans() : 0),
                (fullSponsor.getProjectsBacked() != null ? (int) fullSponsor.getProjectsBacked() : 0));
        view.loadDescription(fullSponsor.getShortDescription());
        view.showHeader();
        view.hideLoader();
    }

    @Override
    public void readMoreClicked() {
        view.loadFullDescription(fullSponsor.getShortDescription());
    }
}
