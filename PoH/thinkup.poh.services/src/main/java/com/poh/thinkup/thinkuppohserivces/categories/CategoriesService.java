package com.poh.thinkup.thinkuppohserivces.categories;

import com.poh.thinkup.thinkupconnection.core.ConnectionError;
import com.poh.thinkup.thinkupconnection.lib.ServiceManager;
import com.poh.thinkup.thinkuppohserivces.base.BaseService;
import com.poh.thinkup.thinkuppohserivces.base.ConnectionCallbackWrapper;
import com.poh.thinkup.thinkuppohserivces.base.ServiceQueue;

import io.reactivex.disposables.Disposable;

public class CategoriesService extends BaseService<CategoriesRequest, CategoriesCallback> {

    @Override
    public void execute(CategoriesRequest request, final CategoriesCallback callback) {
        ServiceManager<CategoriesResult, CategoriesInterface> serviceManager = new ServiceManager<>();
        CategoriesInterface categoriesInterface = serviceManager.serviceInterface(CategoriesInterface.class);
        Disposable disposable = serviceManager.executeService(categoriesInterface.getCategories(),
                new ConnectionCallbackWrapper<CategoriesResult>(callback) {
                    @Override
                    public void onServiceSuccess(CategoriesResult result) {
                        if (checkStatus(result))
                            callback.onCategoriesSuccess(new CategoriesResult.Expose(result.getCategories()));
                    }


                    @Override
                    public void onServiceFail(ConnectionError error) {
                        super.onServiceFail(error);
                        System.out.print(error);
                    }
                });
        ServiceQueue.add(disposable);
    }
}
