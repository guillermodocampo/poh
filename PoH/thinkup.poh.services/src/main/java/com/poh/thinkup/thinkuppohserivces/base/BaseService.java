package com.poh.thinkup.thinkuppohserivces.base;

public abstract class BaseService<T extends BaseRequest, U extends BaseCallback> {
    public abstract void execute(T request, final U callback);
}
