package com.poh.thinkup.thinkuppohserivces.categories;

import com.poh.thinkup.thinkuppohserivces.base.BaseCallback;

public interface CategoriesCallback extends BaseCallback {
    void onCategoriesSuccess(CategoriesResult.Expose result);
}
