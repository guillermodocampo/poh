package com.poh.thinkup.thinkuppohserivces.lib;

import com.poh.thinkup.thinkupconnection.core.ConnectionError;

public class ServiceError {

    private int code;
    private String message;
    private String messageDetail;

    public ServiceError(ConnectionError error) {
        code = error.getCode();
        message = error.getMessage();
        messageDetail = error.getMessageDetail();
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageDetail() {
        return messageDetail;
    }
}
