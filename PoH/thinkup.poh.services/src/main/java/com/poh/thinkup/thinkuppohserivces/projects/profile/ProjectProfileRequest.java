package com.poh.thinkup.thinkuppohserivces.projects.profile;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohserivces.base.BaseRequest;

public class ProjectProfileRequest extends BaseRequest {

    @SerializedName("ProjectID")
    private String projectID;

    public ProjectProfileRequest(String projectID) {
        this.projectID = projectID;
    }
}
