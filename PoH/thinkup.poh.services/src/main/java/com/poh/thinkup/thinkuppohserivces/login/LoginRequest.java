package com.poh.thinkup.thinkuppohserivces.login;

import com.poh.thinkup.thinkuppohserivces.base.BaseRequest;

public class LoginRequest extends BaseRequest {

    private String IPAddress;
    private String UID;
    private String Password;
    private String APIVersion;
    private String UserID;
    private String APIPlatform;

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = IPAddress;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getAPIVersion() {
        return APIVersion;
    }

    public void setAPIVersion(String APIVersion) {
        this.APIVersion = APIVersion;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String UserID) {
        this.UserID = UserID;
    }

    public String getAPIPlatform() {
        return APIPlatform;
    }

    public void setAPIPlatform(String APIPlatform) {
        this.APIPlatform = APIPlatform;
    }

    @Override
    public String toString() {
        return "ClassPojo [IPAddress = " + IPAddress + ", UID = " + UID + ", Password = " + Password + ", APIVersion = " + APIVersion + ", UserID = " + UserID + ", APIPlatform = " + APIPlatform + "]";
    }
}
