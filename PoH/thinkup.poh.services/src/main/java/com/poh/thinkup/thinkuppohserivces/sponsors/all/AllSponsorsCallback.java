package com.poh.thinkup.thinkuppohserivces.sponsors.all;

import com.poh.thinkup.thinkuppohserivces.base.BaseCallback;

public interface AllSponsorsCallback extends BaseCallback {
    void onAllSponsorsSuccess(AllSponsorsResult.Expose sponsorsResult);
}
