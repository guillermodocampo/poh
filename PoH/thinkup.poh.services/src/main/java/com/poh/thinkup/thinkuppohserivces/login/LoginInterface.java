package com.poh.thinkup.thinkuppohserivces.login;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginInterface {
    @POST("session/login")
    Observable<LoginResult> login(@Body LoginRequest loginRequest);
}
