package com.poh.thinkup.thinkuppohserivces.sponsors.all;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohmodel.Sponsor;
import com.poh.thinkup.thinkuppohserivces.base.BaseServiceResult;

import java.util.List;

public class AllSponsorsResult extends BaseServiceResult {

    @SerializedName("sponsors")
    private List<Sponsor> sponsors;

    public List<Sponsor> getSponsors() {
        return sponsors;
    }

    public static class Expose {
        private List<Sponsor> sponsors;

        public Expose(AllSponsorsResult result) {
            sponsors = result.getSponsors();
        }

        public List<Sponsor> getSponsors() {
            return sponsors;
        }
    }
}
