package com.poh.thinkup.thinkuppohserivces.base;

import com.poh.thinkup.thinkupconnection.callbacks.ConnectionCallback;
import com.poh.thinkup.thinkupconnection.core.ConnectionError;
import com.poh.thinkup.thinkuppohserivces.lib.ServiceError;

public abstract class ConnectionCallbackWrapper<T extends BaseServiceResult> implements ConnectionCallback<T> {

    private static final String BAD_STATUS = "-1";

    private BaseCallback callback;

    public ConnectionCallbackWrapper(BaseCallback callback) {
        this.callback = callback;
    }

    public boolean checkStatus(T result) {
        if (result.getStatus() != null && result.getStatus().equals(BAD_STATUS)) {
            callback.onServiceFail(new ServiceError(new ConnectionError(200, "BAD_REQUEST", "")));
            return false;
        }
        return true;
    }

    @Override
    public void onServiceFail(ConnectionError error) {
        callback.onServiceFail(new ServiceError(error));
    }
}
