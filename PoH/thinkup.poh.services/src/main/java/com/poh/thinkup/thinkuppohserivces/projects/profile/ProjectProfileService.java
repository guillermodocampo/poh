package com.poh.thinkup.thinkuppohserivces.projects.profile;

import com.poh.thinkup.thinkupconnection.lib.ServiceManager;
import com.poh.thinkup.thinkuppohserivces.base.BaseService;
import com.poh.thinkup.thinkuppohserivces.base.ConnectionCallbackWrapper;
import com.poh.thinkup.thinkuppohserivces.base.ServiceQueue;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectInterface;

import io.reactivex.disposables.Disposable;

public class ProjectProfileService extends BaseService<ProjectProfileRequest, ProjectProfileCallback> {
    @Override
    public void execute(ProjectProfileRequest request, final ProjectProfileCallback callback) {
        ServiceManager<ProjectProfileResult, ProjectInterface> serviceManager = new ServiceManager<>();
        ProjectInterface profileInterface = serviceManager.serviceInterface(ProjectInterface.class);
        Disposable disposable = serviceManager.executeService(profileInterface.profile(request),
                new ConnectionCallbackWrapper<ProjectProfileResult>(callback) {
                    @Override
                    public void onServiceSuccess(ProjectProfileResult result) {
                        if (checkStatus(result))
                            callback.onProfileSuccess(new ProjectProfileResult.Expose(result));
                    }
                });
        ServiceQueue.add(disposable);
    }
}
