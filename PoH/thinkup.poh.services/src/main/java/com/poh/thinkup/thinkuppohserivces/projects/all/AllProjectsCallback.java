package com.poh.thinkup.thinkuppohserivces.projects.all;

import com.poh.thinkup.thinkuppohserivces.base.BaseCallback;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectsResult;

public interface AllProjectsCallback extends BaseCallback {
    void onAllProjectsSuccess(ProjectsResult.Expose projectsResult);
}
