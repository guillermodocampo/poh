package com.poh.thinkup.thinkuppohserivces.projects.all;

import com.poh.thinkup.thinkupconnection.lib.ServiceManager;
import com.poh.thinkup.thinkuppohserivces.base.BaseRequest;
import com.poh.thinkup.thinkuppohserivces.base.BaseService;
import com.poh.thinkup.thinkuppohserivces.base.ConnectionCallbackWrapper;
import com.poh.thinkup.thinkuppohserivces.base.ServiceQueue;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectInterface;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectsResult;

import io.reactivex.disposables.Disposable;

public class AllProjectService extends BaseService<BaseRequest, AllProjectsCallback> {

    @Override
    public void execute(final BaseRequest request, final AllProjectsCallback callback) {
        ServiceManager<ProjectsResult, ProjectInterface> serviceManager = new ServiceManager<>();
        ProjectInterface loginInterface = serviceManager.serviceInterface(ProjectInterface.class);
        Disposable disposable = serviceManager.executeService(loginInterface.all(),
                new ConnectionCallbackWrapper<ProjectsResult>(callback) {
                    @Override
                    public void onServiceSuccess(ProjectsResult result) {
                        if (checkStatus(result))
                            callback.onAllProjectsSuccess(new ProjectsResult.Expose(result));
                    }
                });
        ServiceQueue.add(disposable);
    }
}
