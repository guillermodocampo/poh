package com.poh.thinkup.thinkuppohserivces.categories;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohmodel.Category;
import com.poh.thinkup.thinkuppohserivces.base.BaseServiceResult;

import java.util.List;

public class CategoriesResult extends BaseServiceResult {

    @SerializedName("data")
    private List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }

    public static class Expose {
        private List<Category> categories;

        public Expose(List<Category> categories) {
            this.categories = categories;
        }

        public List<Category> getCategories() {
            return categories;
        }
    }
}
