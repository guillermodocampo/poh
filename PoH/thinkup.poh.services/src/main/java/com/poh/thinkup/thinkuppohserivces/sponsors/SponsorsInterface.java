package com.poh.thinkup.thinkuppohserivces.sponsors;

import com.poh.thinkup.thinkuppohserivces.sponsors.all.AllSponsorsResult;
import com.poh.thinkup.thinkuppohserivces.sponsors.profile.SponsorDataRequest;
import com.poh.thinkup.thinkuppohserivces.sponsors.profile.SponsorDataResult;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SponsorsInterface {

    @POST("sponsors/allsponsors")
    Observable<AllSponsorsResult> allSponsors();

    @POST("sponsors/getsponsordata")
    Observable<SponsorDataResult> getSponsorData(@Body SponsorDataRequest request);

}
