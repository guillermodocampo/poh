package com.poh.thinkup.thinkuppohserivces.categories;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface CategoriesInterface {

    @GET("category")
    Observable<CategoriesResult> getCategories();
}
