package com.poh.thinkup.thinkuppohserivces.projects.media;

import com.poh.thinkup.thinkuppohserivces.base.BaseCallback;

public interface MediaCallback extends BaseCallback {
    void onMediaSuccess(MediaResult.Expose result);
}
