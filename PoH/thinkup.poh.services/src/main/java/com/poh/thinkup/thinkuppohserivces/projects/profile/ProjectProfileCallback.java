package com.poh.thinkup.thinkuppohserivces.projects.profile;

import com.poh.thinkup.thinkuppohserivces.base.BaseCallback;

public interface ProjectProfileCallback extends BaseCallback {
    void onProfileSuccess(ProjectProfileResult.Expose result);
}
