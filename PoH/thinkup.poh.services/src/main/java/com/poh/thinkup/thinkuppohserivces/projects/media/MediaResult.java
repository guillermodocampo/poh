package com.poh.thinkup.thinkuppohserivces.projects.media;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohmodel.Media;
import com.poh.thinkup.thinkuppohserivces.base.BaseServiceResult;

import java.util.List;

public class MediaResult extends BaseServiceResult {
    @SerializedName("media")
    private List<Media> media;

    public static class Expose {
        private List<Media> media;

        public Expose(MediaResult result) {
            this.media = result.media;
        }

        public List<Media> getMedia() {
            return media;
        }
    }
}
