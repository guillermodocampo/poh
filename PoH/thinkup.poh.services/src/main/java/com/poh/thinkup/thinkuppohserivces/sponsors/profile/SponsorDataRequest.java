package com.poh.thinkup.thinkuppohserivces.sponsors.profile;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohserivces.base.BaseRequest;

public class SponsorDataRequest extends BaseRequest {

    @SerializedName("SponsorID")
    private String sponsorID;

    public SponsorDataRequest(String sponsorID) {
        this.sponsorID = sponsorID;
    }

}
