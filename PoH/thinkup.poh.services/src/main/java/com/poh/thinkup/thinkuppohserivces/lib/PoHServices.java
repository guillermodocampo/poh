package com.poh.thinkup.thinkuppohserivces.lib;

import android.content.Context;
import android.content.ContextWrapper;

import com.pixplicity.easyprefs.library.Prefs;
import com.poh.thinkup.thinkupconnection.lib.TkpInstance;
import com.poh.thinkup.thinkuppohserivces.R;
import com.poh.thinkup.thinkuppohserivces.base.ConstantsValues;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class PoHServices {

    private static void newPoHServices(Configuration configuration) {
        TkpInstance.init(new com.poh.thinkup.thinkupconnection.lib.Configuration.Builder()
                .setLogLevel(configuration.getEnvironment().getLogLevel())
                .setTimeout(configuration.getEnvironment().getTimeout())
                .setUrl(configuration.getEnvironment().getUrl())
                .create());
    }

    private static void newPoHServices(Configuration configuration, SSLSocketFactory sslSocketFactory, X509TrustManager trustManager) {
        TkpInstance.init(new com.poh.thinkup.thinkupconnection.lib.Configuration.Builder()
                .setLogLevel(configuration.getEnvironment().getLogLevel())
                .setTimeout(configuration.getEnvironment().getTimeout())
                .setUrl(configuration.getEnvironment().getUrl())
                .create(), sslSocketFactory, trustManager);
    }

    public static void init(Configuration configuration, Context context) {
        if (configuration.getEnvironment().useCertificate()) createCertificate(configuration, context);
        else newPoHServices(configuration);
        new Prefs.Builder()
                .setContext(context)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(context.getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    public static void updateToken(String token) {
        Prefs.putString(ConstantsValues.AUTH_TOKEN, token);
    }

    private static void createCertificate(Configuration configuration, Context context) {
        InputStream cert = context.getResources().openRawResource(R.raw.poh);
        try {
            // loading CAs from an InputStream
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate ca;
            ca = cf.generateCertificate(cert);

            // creating a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // creating a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // creating an SSLSocketFactory that uses our TrustManager
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, tmf.getTrustManagers(), new java.security.SecureRandom());

            // creating an OkHttpClient that uses our SSLSocketFactory
            newPoHServices(configuration, sslContext.getSocketFactory(), X509());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                cert.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static X509TrustManager X509() {
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };
        return (X509TrustManager) trustAllCerts[0];
    }

}
