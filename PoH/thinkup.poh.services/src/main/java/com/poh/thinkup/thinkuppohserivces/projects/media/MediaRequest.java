package com.poh.thinkup.thinkuppohserivces.projects.media;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohserivces.base.BaseRequest;

public class MediaRequest extends BaseRequest {

    @SerializedName("ProjectID")
    private String projectID;

    public MediaRequest(String projectID) {
        this.projectID = projectID;
    }

    public String getProjectID() {
        return projectID;
    }
}
