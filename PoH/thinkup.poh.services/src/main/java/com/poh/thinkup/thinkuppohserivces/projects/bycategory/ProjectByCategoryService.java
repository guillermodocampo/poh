package com.poh.thinkup.thinkuppohserivces.projects.bycategory;

import com.poh.thinkup.thinkupconnection.lib.ServiceManager;
import com.poh.thinkup.thinkuppohserivces.base.BaseService;
import com.poh.thinkup.thinkuppohserivces.base.ConnectionCallbackWrapper;
import com.poh.thinkup.thinkuppohserivces.base.ServiceQueue;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectInterface;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectsResult;

import io.reactivex.disposables.Disposable;

public class ProjectByCategoryService extends BaseService<ProjectByCategoryRequest, ProjectByCategoryCallback> {

    @Override
    public void execute(ProjectByCategoryRequest request, final ProjectByCategoryCallback callback) {
        ServiceManager<ProjectsResult, ProjectInterface> serviceManager = new ServiceManager<>();
        ProjectInterface loginInterface = serviceManager.serviceInterface(ProjectInterface.class);
        Disposable disposable = serviceManager.executeService(loginInterface.byCategory(request),
                new ConnectionCallbackWrapper<ProjectsResult>(callback) {
                    @Override
                    public void onServiceSuccess(ProjectsResult result) {
                        if (checkStatus(result))
                            callback.onByCategorySuccess(new ProjectsResult.Expose(result));
                    }
                });
        ServiceQueue.add(disposable);
    }
}
