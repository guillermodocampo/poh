package com.poh.thinkup.thinkuppohserivces.projects.bycategory;

import com.poh.thinkup.thinkuppohserivces.base.BaseCallback;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectsResult;

public interface ProjectByCategoryCallback extends BaseCallback {
    void onByCategorySuccess(ProjectsResult.Expose projectsResult);
}
