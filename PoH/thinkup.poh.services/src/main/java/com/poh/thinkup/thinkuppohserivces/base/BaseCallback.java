package com.poh.thinkup.thinkuppohserivces.base;

import com.poh.thinkup.thinkuppohserivces.lib.ServiceError;

public interface BaseCallback {
    void onServiceFail(ServiceError error);
}
