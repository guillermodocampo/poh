package com.poh.thinkup.thinkuppohserivces.sponsors.profile;

import com.poh.thinkup.thinkupconnection.lib.ServiceManager;
import com.poh.thinkup.thinkuppohserivces.base.BaseService;
import com.poh.thinkup.thinkuppohserivces.base.ConnectionCallbackWrapper;
import com.poh.thinkup.thinkuppohserivces.base.ServiceQueue;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectInterface;
import com.poh.thinkup.thinkuppohserivces.sponsors.SponsorsInterface;

import io.reactivex.disposables.Disposable;

public class SponsorDataService extends BaseService<SponsorDataRequest, SponsorDataCallback> {

    @Override
    public void execute(SponsorDataRequest request, final SponsorDataCallback callback) {
        ServiceManager<SponsorDataResult, SponsorsInterface> serviceManager = new ServiceManager<>();
        SponsorsInterface sponsorDataInterface = serviceManager.serviceInterface(SponsorsInterface.class);
        Disposable disposable = serviceManager.executeService(sponsorDataInterface.getSponsorData(request),
                new ConnectionCallbackWrapper<SponsorDataResult>(callback) {
                    @Override
                    public void onServiceSuccess(SponsorDataResult result) {
                        if (checkStatus(result))
                            callback.onSponsorDataSuccess(new SponsorDataResult.Expose(result));
                    }
                });
        ServiceQueue.add(disposable);
    }
}
