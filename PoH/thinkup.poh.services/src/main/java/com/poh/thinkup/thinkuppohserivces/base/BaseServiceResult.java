package com.poh.thinkup.thinkuppohserivces.base;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkupconnection.core.ServiceResult;

public class BaseServiceResult extends ServiceResult {

    @SerializedName("status")
    private String status;

    public String getStatus() {
        return status;
    }
}
