package com.poh.thinkup.thinkuppohserivces.lib;

import com.poh.thinkup.thinkupconnection.lib.Configuration.LogLevel;

public class Configuration {

    private Environment environment;

    public Configuration(Environment environment) {
        this.environment = environment;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public enum Environment {
        PRODUCTION(30, UrlConfig.PROD_URL, LogLevel.NONE),
        DEVELOPMENT(30, UrlConfig.DEV_URL, LogLevel.BODY);

        private int timeout;
        private String url;
        private LogLevel logLevel;
        private boolean useCertificate = false;

        Environment(int timeout, String url, LogLevel logLevel) {
            this.timeout = timeout;
            this.url = url;
            this.logLevel = logLevel;
        }

        public int getTimeout() {
            return timeout;
        }

        public String getUrl() {
            return url;
        }

        public LogLevel getLogLevel() {
            return logLevel;
        }

        public boolean useCertificate() {
            return useCertificate;
        }
    }
}
