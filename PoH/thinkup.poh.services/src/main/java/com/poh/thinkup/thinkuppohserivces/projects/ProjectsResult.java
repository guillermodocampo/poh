package com.poh.thinkup.thinkuppohserivces.projects;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohmodel.Project;
import com.poh.thinkup.thinkuppohserivces.base.BaseServiceResult;

import java.util.List;

public class ProjectsResult extends BaseServiceResult {

    @SerializedName("projects")
    private List<Project> projects;

    public List<Project> getProjects() {
        return projects;
    }

    public static class Expose {
        private List<Project> projects;

        public Expose(ProjectsResult result) {
            projects = result.getProjects();
        }

        public List<Project> getProjects() {
            return projects;
        }
    }
}
