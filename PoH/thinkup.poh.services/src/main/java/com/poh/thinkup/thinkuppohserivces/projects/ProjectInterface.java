package com.poh.thinkup.thinkuppohserivces.projects;

import com.poh.thinkup.thinkuppohserivces.projects.bycategory.ProjectByCategoryRequest;
import com.poh.thinkup.thinkuppohserivces.projects.media.MediaRequest;
import com.poh.thinkup.thinkuppohserivces.projects.media.MediaResult;
import com.poh.thinkup.thinkuppohserivces.projects.profile.ProjectProfileRequest;
import com.poh.thinkup.thinkuppohserivces.projects.profile.ProjectProfileResult;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ProjectInterface {

    @POST("projects/all")
    Observable<ProjectsResult> all();

    @POST("projects/bycategory")
    Observable<ProjectsResult> byCategory(@Body ProjectByCategoryRequest request);

    @POST("projects/profile")
    Observable<ProjectProfileResult> profile(@Body ProjectProfileRequest request);

    @POST("projects/media")
    Observable<MediaResult> media(@Body MediaRequest request);
}
