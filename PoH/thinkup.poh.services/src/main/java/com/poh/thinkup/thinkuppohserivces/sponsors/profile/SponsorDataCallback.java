package com.poh.thinkup.thinkuppohserivces.sponsors.profile;

import com.poh.thinkup.thinkuppohserivces.base.BaseCallback;

public interface SponsorDataCallback extends BaseCallback{

    void onSponsorDataSuccess(SponsorDataResult.Expose sponsorDataResult);
}
