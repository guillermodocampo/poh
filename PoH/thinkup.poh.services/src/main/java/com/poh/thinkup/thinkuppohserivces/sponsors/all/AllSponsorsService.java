package com.poh.thinkup.thinkuppohserivces.sponsors.all;

import com.poh.thinkup.thinkupconnection.lib.ServiceManager;
import com.poh.thinkup.thinkuppohserivces.base.BaseService;
import com.poh.thinkup.thinkuppohserivces.base.ConnectionCallbackWrapper;
import com.poh.thinkup.thinkuppohserivces.base.ServiceQueue;
import com.poh.thinkup.thinkuppohserivces.sponsors.SponsorsInterface;

import io.reactivex.disposables.Disposable;

public class AllSponsorsService extends BaseService<AllSponsorsRequest, AllSponsorsCallback> {

    @Override
    public void execute(AllSponsorsRequest request, final AllSponsorsCallback callback) {

        ServiceManager<AllSponsorsResult, SponsorsInterface> serviceManager = new ServiceManager<>();
        SponsorsInterface allSponsorsInterface = serviceManager.serviceInterface(SponsorsInterface.class);
        Disposable disposable = serviceManager.executeService(allSponsorsInterface.allSponsors(),
                new ConnectionCallbackWrapper<AllSponsorsResult>(callback) {
                    @Override
                    public void onServiceSuccess(AllSponsorsResult result) {
                        if (checkStatus(result))
                            callback.onAllSponsorsSuccess(new AllSponsorsResult.Expose(result));
                    }
                });
        ServiceQueue.add(disposable);

    }
}
