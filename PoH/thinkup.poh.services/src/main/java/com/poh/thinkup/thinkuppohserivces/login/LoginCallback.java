package com.poh.thinkup.thinkuppohserivces.login;

import com.poh.thinkup.thinkuppohserivces.base.BaseCallback;

public interface LoginCallback extends BaseCallback {
    void onLoginSuccess(LoginResult.Expose result);
}
