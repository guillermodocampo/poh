package com.poh.thinkup.thinkuppohserivces.projects.profile;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohmodel.FullProject;
import com.poh.thinkup.thinkuppohserivces.base.BaseServiceResult;

public class ProjectProfileResult extends BaseServiceResult {

    @SerializedName("project")
    private FullProject project;

    public FullProject getProject() {
        return project;
    }

    public static class Expose {
        private FullProject project;

        public Expose(ProjectProfileResult result) {
            project = result.getProject();
        }

        public FullProject getProject() {
            return project;
        }
    }
}
