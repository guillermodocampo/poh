package com.poh.thinkup.thinkuppohserivces.sponsors.profile;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohmodel.FullSponsor;
import com.poh.thinkup.thinkuppohserivces.base.BaseServiceResult;

public class SponsorDataResult extends BaseServiceResult {

    @SerializedName("sponsor")
    private FullSponsor sponsor;

    public FullSponsor getSponsor() {
        return sponsor;
    }

    public static class Expose {
        private FullSponsor sponsor;

        public Expose(SponsorDataResult result) {
            sponsor = result.getSponsor();
        }

        public FullSponsor getSponsor() {
            return sponsor;
        }
    }

}
