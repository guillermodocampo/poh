package com.poh.thinkup.thinkuppohserivces.projects.bycategory;

import com.google.gson.annotations.SerializedName;
import com.poh.thinkup.thinkuppohserivces.base.BaseRequest;

public class ProjectByCategoryRequest extends BaseRequest {

    @SerializedName("Category")
    private String category;

    public ProjectByCategoryRequest(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }
}
