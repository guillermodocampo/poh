package com.poh.thinkup.thinkuppohserivces.projects.media;

import com.poh.thinkup.thinkupconnection.lib.ServiceManager;
import com.poh.thinkup.thinkuppohserivces.base.BaseService;
import com.poh.thinkup.thinkuppohserivces.base.ConnectionCallbackWrapper;
import com.poh.thinkup.thinkuppohserivces.base.ServiceQueue;
import com.poh.thinkup.thinkuppohserivces.projects.ProjectInterface;

import io.reactivex.disposables.Disposable;

public class MediaService extends BaseService<MediaRequest, MediaCallback> {
    @Override
    public void execute(MediaRequest request, final MediaCallback callback) {
        ServiceManager<MediaResult, ProjectInterface> serviceManager = new ServiceManager<>();
        ProjectInterface projectInterface = serviceManager.serviceInterface(ProjectInterface.class);
        Disposable disposable = serviceManager.executeService(projectInterface.media(request),
                new ConnectionCallbackWrapper<MediaResult>(callback) {
                    @Override
                    public void onServiceSuccess(MediaResult result) {
                        if (checkStatus(result))
                            callback.onMediaSuccess(new MediaResult.Expose(result));
                    }
                });
        ServiceQueue.add(disposable);
    }
}
