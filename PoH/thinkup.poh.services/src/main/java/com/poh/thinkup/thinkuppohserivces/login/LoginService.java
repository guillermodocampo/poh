package com.poh.thinkup.thinkuppohserivces.login;

import com.poh.thinkup.thinkupconnection.lib.ServiceManager;
import com.poh.thinkup.thinkuppohserivces.base.BaseService;
import com.poh.thinkup.thinkuppohserivces.base.ConnectionCallbackWrapper;
import com.poh.thinkup.thinkuppohserivces.base.ServiceQueue;

import io.reactivex.disposables.Disposable;

public class LoginService extends BaseService<LoginRequest, LoginCallback> {

    @Override
    public void execute(LoginRequest request, final LoginCallback callback) {
        ServiceManager<LoginResult, LoginInterface> serviceManager = new ServiceManager<>();
        LoginInterface loginInterface = serviceManager.serviceInterface(LoginInterface.class);
        Disposable disposable = serviceManager.executeService(loginInterface.login(request),
                new ConnectionCallbackWrapper<LoginResult>(callback) {
                    @Override
                    public void onServiceSuccess(LoginResult result) {
                        if (checkStatus(result))
                            callback.onLoginSuccess(new LoginResult.Expose(result));
                    }
                });
        ServiceQueue.add(disposable);
    }

}
