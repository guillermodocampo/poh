package com.poh.thinkup.thinkuppohmodel;

import com.google.gson.annotations.SerializedName;

public class FullProject {
    @SerializedName("Active")
    private Boolean active;
    @SerializedName("ProjectID")
    private String projectID;
    @SerializedName("Image")
    private String image;
    @SerializedName("Title")
    private String title;
    @SerializedName("ShortDescription")
    private String shortDescription;
    @SerializedName("ProjectOwner")
    private String projectOwner;
    @SerializedName("Location")
    private String location;
    @SerializedName("Category")
    private String category;
    @SerializedName("ProjectGoal")
    private Integer projectGoal;
    @SerializedName("StartDate")
    private String startDate;
    @SerializedName("ProjectDeadline")
    private String projectDeadline;
    @SerializedName("ProjectDescription")
    private String projectDescription;
    @SerializedName("ProjectViewers")
    private Object projectViewers;
    @SerializedName("AUReceived")
    private Double aUReceived;
    @SerializedName("USDReceived")
    private Double uSDReceived;
    @SerializedName("ProjectGoalnAU")
    private Integer projectGoalnAU;
    @SerializedName("Media")
    private Object media;
    @SerializedName("sponsors")
    private Object sponsors;
    @SerializedName("Humans")
    private Object humans;
    @SerializedName("Tags")
    private Object tags;

    public Boolean getActive() {
        return active;
    }

    public String getProjectID() {
        return projectID;
    }

    public String getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getProjectOwner() {
        return projectOwner;
    }

    public String getLocation() {
        return location;
    }

    public String getCategory() {
        return category;
    }

    public Integer getProjectGoal() {
        return projectGoal;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getProjectDeadline() {
        return projectDeadline;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public Object getProjectViewers() {
        return projectViewers;
    }

    public Double getAUReceived() {
        return aUReceived;
    }

    public Double getUSDReceived() {
        return uSDReceived;
    }

    public Integer getProjectGoalnAU() {
        return projectGoalnAU;
    }

    public Object getMedia() {
        return media;
    }

    public Object getSponsors() {
        return sponsors;
    }

    public Object getHumans() {
        return humans;
    }

    public Object getTags() {
        return tags;
    }
}
