package com.poh.thinkup.thinkuppohmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Category implements Parcelable {

    @SerializedName("shown_name")
    private String name;
    @SerializedName("AURecived")
    private double aURecived;
    @SerializedName("USDRecived")
    private double uSDRecived;
    @SerializedName("projects")
    private String projects;

    protected Category(Parcel in) {
        name = in.readString();
        aURecived = in.readDouble();
        uSDRecived = in.readDouble();
        projects = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(aURecived);
        dest.writeDouble(uSDRecived);
        dest.writeString(projects);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public String getName() {
        return name;
    }

    public double getaURecived() {
        return aURecived;
    }

    public double getuSDRecived() {
        return uSDRecived;
    }

    public String getProjects() {
        return projects;
    }
}
