package com.poh.thinkup.thinkuppohmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Human {

    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Avatar")
    @Expose
    private String avatar;
    @SerializedName("AUGiven")
    @Expose
    private Double aUGiven;
    @SerializedName("USDGiven")
    @Expose
    private Double uSDGiven;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Double getAUGiven() {
        return aUGiven;
    }

    public void setAUGiven(Double aUGiven) {
        this.aUGiven = aUGiven;
    }

    public Double getUSDGiven() {
        return uSDGiven;
    }

    public void setUSDGiven(Double uSDGiven) {
        this.uSDGiven = uSDGiven;
    }

}
