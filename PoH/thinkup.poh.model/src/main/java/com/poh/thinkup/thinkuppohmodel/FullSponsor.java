package com.poh.thinkup.thinkuppohmodel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FullSponsor implements Serializable {

    @SerializedName("UserID")
    private String userID;
    @SerializedName("Email")
    private String email;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("LastName")
    private String lastName;
    @SerializedName("Country")
    private String country;
    @SerializedName("ShortDescription")
    private String shortDescription;
    @SerializedName("Avatar")
    private String avatar;
    @SerializedName("AUGiven")
    private Integer aUGiven;
    @SerializedName("SponsorType")
    private Integer sponsorType;
    @SerializedName("JoinDate")
    private String joinDate;
    @SerializedName("ProjectsBacked")
    private Object projectsBacked;
    @SerializedName("Facebook")
    private String facebook;
    @SerializedName("TwitterID")
    private Object twitterID;
    @SerializedName("Type")
    private String type;
    @SerializedName("Active")
    private Boolean active;
    @SerializedName("IsSponsor")
    private Boolean isSponsor;
    @SerializedName("Humans")
    private Object humans;
    @SerializedName("ActiveAds")
    private Object activeAds;
    @SerializedName("InactiveAds")
    private Object inactiveAds;
    @SerializedName("AURemaining")
    private Integer aURemaining;
    @SerializedName("USDRemaining")
    private Integer uSDRemaining;
    @SerializedName("TimeUsersWatched")
    private Object timeUsersWatched;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getAUGiven() {
        return aUGiven;
    }

    public void setAUGiven(Integer aUGiven) {
        this.aUGiven = aUGiven;
    }

    public Integer getSponsorType() {
        return sponsorType;
    }

    public void setSponsorType(Integer sponsorType) {
        this.sponsorType = sponsorType;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public Object getProjectsBacked() {
        return projectsBacked;
    }

    public void setProjectsBacked(Object projectsBacked) {
        this.projectsBacked = projectsBacked;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public Object getTwitterID() {
        return twitterID;
    }

    public void setTwitterID(Object twitterID) {
        this.twitterID = twitterID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getIsSponsor() {
        return isSponsor;
    }

    public void setIsSponsor(Boolean isSponsor) {
        this.isSponsor = isSponsor;
    }

    public Object getHumans() {
        return humans;
    }

    public void setHumans(Object humans) {
        this.humans = humans;
    }

    public Object getActiveAds() {
        return activeAds;
    }

    public void setActiveAds(Object activeAds) {
        this.activeAds = activeAds;
    }

    public Object getInactiveAds() {
        return inactiveAds;
    }

    public void setInactiveAds(Object inactiveAds) {
        this.inactiveAds = inactiveAds;
    }

    public Integer getAURemaining() {
        return aURemaining;
    }

    public void setAURemaining(Integer aURemaining) {
        this.aURemaining = aURemaining;
    }

    public Integer getUSDRemaining() {
        return uSDRemaining;
    }

    public void setUSDRemaining(Integer uSDRemaining) {
        this.uSDRemaining = uSDRemaining;
    }

    public Object getTimeUsersWatched() {
        return timeUsersWatched;
    }

    public void setTimeUsersWatched(Object timeUsersWatched) {
        this.timeUsersWatched = timeUsersWatched;
    }

}
