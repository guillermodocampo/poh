package com.poh.thinkup.thinkuppohmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Sponsor implements Serializable{

    @SerializedName("SponsorID")
    @Expose
    private String sponsorID;
    @SerializedName("SponsorName")
    @Expose
    private String sponsorName;
    @SerializedName("Avatar")
    @Expose
    private String avatar;
    @SerializedName("AUDonated")
    @Expose
    private Double aUDonated;
    @SerializedName("USDDonated")
    @Expose
    private Double uSDDonated;

    public String getSponsorID() {
        return sponsorID;
    }

    public void setSponsorID(String sponsorID) {
        this.sponsorID = sponsorID;
    }

    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Double getAUDonated() {
        return aUDonated;
    }

    public void setAUDonated(Double aUDonated) {
        this.aUDonated = aUDonated;
    }

    public Double getUSDDonated() {
        return uSDDonated;
    }

    public void setUSDDonated(Double uSDDonated) {
        this.uSDDonated = uSDDonated;
    }

}
