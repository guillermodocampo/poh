package com.poh.thinkup.thinkuppohmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Project implements Serializable{

    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("ProjectID")
    @Expose
    private String projectID;
    @SerializedName("ProjectImage")
    @Expose
    private String projectImage;
    @SerializedName("ProjectOwner")
    @Expose
    private Object projectOwner;
    @SerializedName("ShortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("TotalSponsors")
    @Expose
    private Integer totalSponsors;
    @SerializedName("TotalAUGiven")
    @Expose
    private Double totalAUGiven;
    @SerializedName("TotalUSDGiven")
    @Expose
    private Double totalUSDGiven;
    @SerializedName("ProjectImageIndex")
    @Expose
    private Object projectImageIndex;
    @SerializedName("ProjectTags")
    @Expose
    private String projectTags;
    @SerializedName("Followers")
    @Expose
    private Integer followers;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getProjectImage() {
        return projectImage;
    }

    public void setProjectImage(String projectImage) {
        this.projectImage = projectImage;
    }

    public Object getProjectOwner() {
        return projectOwner;
    }

    public void setProjectOwner(Object projectOwner) {
        this.projectOwner = projectOwner;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Integer getTotalSponsors() {
        return totalSponsors;
    }

    public void setTotalSponsors(Integer totalSponsors) {
        this.totalSponsors = totalSponsors;
    }

    public Double getTotalAUGiven() {
        return totalAUGiven;
    }

    public void setTotalAUGiven(Double totalAUGiven) {
        this.totalAUGiven = totalAUGiven;
    }

    public Double getTotalUSDGiven() {
        return totalUSDGiven;
    }

    public void setTotalUSDGiven(Double totalUSDGiven) {
        this.totalUSDGiven = totalUSDGiven;
    }

    public Object getProjectImageIndex() {
        return projectImageIndex;
    }

    public void setProjectImageIndex(Object projectImageIndex) {
        this.projectImageIndex = projectImageIndex;
    }

    public String getProjectTags() {
        return projectTags;
    }

    public void setProjectTags(String projectTags) {
        this.projectTags = projectTags;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

}
