package com.poh.thinkup.thinkupconnection.lib;

import com.poh.thinkup.thinkupconnection.callbacks.ConnectionCallback;
import com.poh.thinkup.thinkupconnection.core.ConnectionError;
import com.poh.thinkup.thinkupconnection.core.NoNetworkException;
import com.poh.thinkup.thinkupconnection.core.ServiceResult;

import java.io.Serializable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;

class ConnectionManagerImpl<T extends ServiceResult, W> implements ConnectionManager<T, W>, Serializable {

    private Retrofit retrofit;

    public ConnectionManagerImpl(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public W createRequest(final Class<W> service) {
        return retrofit.create(service);
    }

    @Override
    public Disposable executeService(final Observable<T> observable, final ConnectionCallback<T> callback) {
        return observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<T>() {
                    @Override
                    public void accept(@NonNull T result) throws Exception {
                        callback.onServiceSuccess(result);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) {
                        ConnectionError connectionError;
                        if (throwable instanceof HttpException) {
                            connectionError = httpConnectionError((HttpException) throwable);
                        } else {
                            connectionError
                                    = new ConnectionError((throwable instanceof NoNetworkException)
                                    ? ConnectionError.NO_NETWORK : ConnectionError.UNAVAILABLE_CONNECTION, "", "");
                        }
                        callback.onServiceFail(connectionError);
                    }
                });
    }

    private ConnectionError httpConnectionError(HttpException e) {
        return new ConnectionError(e.code(), e.message(), e.getMessage());
    }
}
