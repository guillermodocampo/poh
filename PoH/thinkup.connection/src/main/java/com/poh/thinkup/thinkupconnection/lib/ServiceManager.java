package com.poh.thinkup.thinkupconnection.lib;

import com.poh.thinkup.thinkupconnection.callbacks.ConnectionCallback;
import com.poh.thinkup.thinkupconnection.core.ServiceResult;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class ServiceManager<T extends ServiceResult, W> {

    ConnectionManager<T, W> connectionManager;

    public ServiceManager() {
        this.connectionManager = new ConnectionManagerImpl<>(TkpInstance.getInstance().getConnectionManagerProvider());
    }

    public W serviceInterface(Class<W> returnClass) {
        return connectionManager.createRequest(returnClass);
    }

    public Disposable executeService(final Observable<T> observable, final ConnectionCallback<T> callback) {
        return connectionManager.executeService(observable, callback);
    }

}
