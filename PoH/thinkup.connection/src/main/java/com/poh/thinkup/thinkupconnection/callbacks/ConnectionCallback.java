package com.poh.thinkup.thinkupconnection.callbacks;

import com.poh.thinkup.thinkupconnection.core.ConnectionError;
import com.poh.thinkup.thinkupconnection.core.ServiceResult;

public interface ConnectionCallback<T extends ServiceResult> {
    void onServiceSuccess(T result);
    void onServiceFail(ConnectionError error);
}