package com.poh.thinkup.thinkupconnection.lib;

import com.poh.thinkup.thinkupconnection.callbacks.ConnectionCallback;
import com.poh.thinkup.thinkupconnection.core.ServiceResult;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

interface ConnectionManager<T extends ServiceResult, W> {
    W createRequest(final Class<W> service);
    Disposable executeService(final Observable<T> observable, final ConnectionCallback<T> callback);
}