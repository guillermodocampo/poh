package com.poh.thinkup.thinkupconnection.lib;


import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

import retrofit2.Retrofit;

public class TkpInstance {

    private static TkpInstance instance;

    private ConnectionManagerProvider connectionManagerProvider;

    public static void init(Configuration configuration) {
        instance = new TkpInstance(configuration);
    }

    public static void init(Configuration configuration, SSLSocketFactory sslSocketFactory, X509TrustManager trustManager) {
        instance = new TkpInstance(configuration, sslSocketFactory, trustManager);
    }

    public static TkpInstance getInstance() {
        if (instance != null) return instance;
        throw new RuntimeException("TkpInstance instance must be initialized");
    }

    private TkpInstance(Configuration configuration) {
        this.connectionManagerProvider = new ConnectionManagerProvider(configuration);
    }

    private TkpInstance(Configuration configuration, SSLSocketFactory sslSocketFactory, X509TrustManager trustManager) {
        this.connectionManagerProvider = new ConnectionManagerProvider(configuration, sslSocketFactory, trustManager);
    }

    protected Retrofit getConnectionManagerProvider() {
        return connectionManagerProvider.getRetrofit();
    }
}
