package com.poh.thinkup.thinkupconnection.lib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.poh.thinkup.thinkupconnection.adapters.DoubleTypeAdapter;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

class ConnectionManagerProvider {

    private static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    private OkHttpClient.Builder httpClientBuilder;
    private Gson gson;
    private Retrofit retrofit;

    public ConnectionManagerProvider(Configuration configuration) {
        this.httpClientBuilder = provideOkHttpClient(configuration);
        this.gson = provideGson();
        this.retrofit = provideRetrofit(configuration);
    }

    public ConnectionManagerProvider(Configuration configuration, SSLSocketFactory sslSocketFactory, X509TrustManager trustManager) {
        this.httpClientBuilder = provideOkHttpClient(configuration, sslSocketFactory, trustManager);
        this.gson = provideGson();
        this.retrofit = provideRetrofit(configuration);
    }

    private Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder()
                .setVersion(1)
                .setDateFormat(SERVER_DATE_FORMAT)
                .registerTypeAdapter(Double.class, new DoubleTypeAdapter())
                .registerTypeAdapter(double.class, new DoubleTypeAdapter());
        return gsonBuilder.create();
    }

    private OkHttpClient.Builder provideOkHttpClient(Configuration configuration) {
        if (httpClientBuilder == null) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            httpClientBuilder = new OkHttpClient.Builder()
                    .connectTimeout(configuration.getTimeout(), TimeUnit.SECONDS)
                    .writeTimeout(configuration.getTimeout(), TimeUnit.SECONDS)
                    .readTimeout(configuration.getTimeout(), TimeUnit.SECONDS);

            loggingInterceptor.setLevel(configuration.getLogLevel());

            httpClientBuilder.addInterceptor(loggingInterceptor);
        }
        return httpClientBuilder;
    }

    private OkHttpClient.Builder provideOkHttpClient(Configuration configuration, SSLSocketFactory sslSocketFactory, X509TrustManager trustManager) {
        if (httpClientBuilder == null) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            httpClientBuilder = new OkHttpClient.Builder()
                    .connectTimeout(configuration.getTimeout(), TimeUnit.SECONDS)
                    .writeTimeout(configuration.getTimeout(), TimeUnit.SECONDS)
                    .readTimeout(configuration.getTimeout(), TimeUnit.SECONDS)
                    .sslSocketFactory(sslSocketFactory, trustManager)
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    });

            loggingInterceptor.setLevel(configuration.getLogLevel());

            httpClientBuilder.addInterceptor(loggingInterceptor);
        }
        return httpClientBuilder;
    }

    private Retrofit provideRetrofit(Configuration configuration) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(configuration.getUrl())
                .client(httpClientBuilder.build())
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
