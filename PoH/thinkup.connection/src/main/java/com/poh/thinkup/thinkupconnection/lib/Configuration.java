package com.poh.thinkup.thinkupconnection.lib;

import okhttp3.logging.HttpLoggingInterceptor;

public class Configuration {
    private int timeout;
    private String url;
    private LogLevel logLevel;

    public int getTimeout() {
        return timeout;
    }

    public String getUrl() {
        return url;
    }

    public HttpLoggingInterceptor.Level getLogLevel() {
        switch (logLevel) {
            case BODY:
                return HttpLoggingInterceptor.Level.BODY;
            case BASIC:
                return HttpLoggingInterceptor.Level.BASIC;
            case HEADERS:
                return HttpLoggingInterceptor.Level.HEADERS;
            case NONE:
            default:
                return HttpLoggingInterceptor.Level.NONE;
        }
    }

    private Configuration(int timeout, String url, LogLevel logLevel) {
        this.timeout = timeout;
        this.url = url;
        this.logLevel = logLevel;
    }

    public static class Builder {
        private int timeout;
        private String url;
        private LogLevel logLevel;

        public Builder() {
        }

        public Builder setTimeout(int timeout) {
            this.timeout = timeout;
            return this;
        }

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setLogLevel(LogLevel logLevel) {
            this.logLevel = logLevel;
            return this;
        }

        public Configuration create() {
            return new Configuration(timeout, url, logLevel);
        }
    }

    public enum LogLevel {
        NONE,
        BASIC,
        HEADERS,
        BODY
    }
}
