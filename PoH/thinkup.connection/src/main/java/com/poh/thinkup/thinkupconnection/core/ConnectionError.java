package com.poh.thinkup.thinkupconnection.core;

public class ConnectionError {

    public static final int UNAVAILABLE_CONNECTION = 5;
    public static final int NO_NETWORK = 6;

    private int code;
    private String message;
    private String messageDetail;

    public ConnectionError(int code, String message, String messageDetail) {
        this.code = code;
        this.message = message;
        this.messageDetail = messageDetail;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageDetail() {
        return messageDetail;
    }
}
